import React, { useEffect , useState} from 'react';
import { Link as RouterLink, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Avatar,
  Box,
  Button,
  Divider,
  Drawer,
  Hidden,
  List,
  Typography,
  makeStyles
} from '@material-ui/core';
import {
  AlertCircle as AlertCircleIcon,
  BarChart as BarChartIcon,
  Lock as LockIcon,
  ShoppingBag as ShoppingBagIcon,
  User as UserIcon,
  UserPlus as UserPlusIcon,
  Users as UsersIcon
} from 'react-feather';
import NavItem from './NavItem';
import {connect} from 'react-redux'
import {addUser } from '../../../js/Action/index'
import SupervisorAccountOutlinedIcon from '@material-ui/icons/SupervisorAccountOutlined';
const user = {
  avatar: '/static/images/avatars/avatar_6.png',
  jobTitle: 'Senior Developer',
  name: 'Katarina Smith'
};

function mapDispatchToProps(dispatch) {
  return {
    addUser: (type, user) => dispatch(addUser(type, user))
  };
}

const items = [
  {
    href: '/app/dashboard' ,
    icon: BarChartIcon,
    title: 'Dashboard'
  },
  {
    href: '/app/account',
    icon: UserIcon,
    title: 'Account'
  }
];

const useStyles = makeStyles(() => ({
  mobileDrawer: {
    width: 256
  },
  desktopDrawer: {
    width: 256,
    top: 64,
    height: 'calc(100% - 64px)'
  },
  avatar: {
    cursor: 'pointer',
    width: 64,
    height: 64
  }
}));

const NavBar = ({ onMobileClose, openMobile ,user, addUser}) => {


  // console.log(user[0].id);
  const classes = useStyles();
  const location = useLocation();
  const [admin, setAdmin] = useState(false);

  const isAdmin=(userData)=>{
    if(userData){
      console.log(userData['userName'])
      if(userData['userName'] == "Girish"){
        setAdmin(true)
      }
    }
  }
  
  const handleLogout=()=>{
    window.localStorage.clear();
    addUser('LOGOUT', { })
  }

  useEffect(() => {
    isAdmin(user[0]);
    if (openMobile && onMobileClose) {
      onMobileClose();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location.pathname]);

  const content = (
    <Box
      height="100%"
      display="flex"
      flexDirection="column"
    >
      <Box
        alignItems="center"
        display="flex"
        flexDirection="column"
        p={2}
      >
        <Avatar
          className={classes.avatar}
          component={RouterLink}
          src={user.avatar}
          to="/app/account"
        />
        <Typography
          className={classes.name}
          color="textPrimary"
          variant="h5"
        >
          {user.name}
        </Typography>
        <Typography
          color="textSecondary"
          variant="body2"
        >
          {user.jobTitle}
        </Typography>
      </Box>
      <Divider />
      <Box p={2}>
        <List>
          {items.map((item) => (
            <NavItem
              href={item.href}
              key={item.title}
              title={item.title}
              icon={item.icon}
            />
          ))}
         {admin &&
         <NavItem
              href='/app/admin'
              key='Admin'
              title= 'Admin'
              icon={UsersIcon}
            />
         }
  
        {user && user[0] &&
        <RouterLink to ='/'>
        <LockIcon></LockIcon>
         <Button onClick={handleLogout}>Logout</Button>
         </RouterLink> 
}
        </List>
      </Box>
      <Box flexGrow={1} />
    </Box>
  );

  return (
    <>
      <Hidden lgUp>
        <Drawer
          anchor="left"
          classes={{ paper: classes.mobileDrawer }}
          onClose={onMobileClose}
          open={openMobile}
          variant="temporary"
        >
          {content}
        </Drawer>
      </Hidden>
      <Hidden mdDown>
        <Drawer
          anchor="left"
          classes={{ paper: classes.desktopDrawer }}
          open
          variant="persistent"
        >
          {content}
        </Drawer>
      </Hidden>
    </>
  );
};

NavBar.propTypes = {
  onMobileClose: PropTypes.func,
  openMobile: PropTypes.bool
};

NavBar.defaultProps = {
  onMobileClose: () => {},
  openMobile: false
};
 const mapStateToProps=(state)=>{
  return { user : state.user }
 }
export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
