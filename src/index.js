import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Router } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import App from './App';
import {Provider} from 'react-redux'
import { createStore } from "redux";
import rootReducer from "./js/Reducer/index";
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-mui'
const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state');
    if(serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (e) {
    return undefined;
  }
};

const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('state', serializedState);
  } catch (e) {
    // Ignore write errors;
  }
};

const peristedState = loadState();



const store = createStore(rootReducer , peristedState);

store.subscribe(() => {
  console.log( localStorage.getItem('state'))
  window.localStorage.clear();
  saveState(store.getState());
});

ReactDOM.render((
  <Provider store={store}>
  <BrowserRouter>
  <AlertProvider template={AlertTemplate}>
    <App />
    </AlertProvider>
  </BrowserRouter>
  </Provider>
), document.getElementById('root'));

serviceWorker.unregister();
