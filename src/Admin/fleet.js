import React, { useState, useEffect } from "react";
import Axios from "axios";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import FleetForm from "./Form/FleetForm";
import { Link } from "react-router-dom";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import {  FormLabel, FormGroup, FormControlLabel, TextField, Grid } from '@material-ui/core'
const useStyles = makeStyles({
  root: {
    width: "100%",
    //   alignItems : 'center',
    //   justifyContent : 'center'
  },
  container: {
    maxHeight: 440,
  },
});

const FleetTable = () => {
  const initialValues={
    id : 0,
    airportName : '',
    code: '',
    country : '',
    name: '',
    status: 'active'

}
  const classes = useStyles();
  const [fleet, setFleet] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(4);
  const [uFleet, setUFleet] = useState({});
  const [open, setOpen] = useState(false);
  const [eFleet, setEFleet] = useState(initialValues);
  // const [values, setValues] = useState()
  const [openForm, setOpenForm] = useState(false);
  const handleInputChange =(event)=>{
    // const [name, value] = event.target
    setEFleet({
        ...eFleet, [event.target.name]:  event.target.value
    })

}


const handleClick =async()=>{
  console.log(eFleet)
     await Axios.put('http://localhost:9001/flight/fleet/update', eFleet )
        .then((response)=>{
            console.log(response);
            setOpenForm(false)
        })

        getFleet();

}
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  // const handleClick = (id) => {
  //   console.log(id);
  // };
  const handleChangeEdit = async (idx) => {
    let index = fleet[idx].id;
    console.log(index);

    await Axios.get(
      "http://localhost:9001/flight/fleet/find/id/" + index
    ).then((response) => {
      setEFleet(response.data);
      console.log(response);
    });
    setOpenForm(true)
  };
  const handleFormClose = () => {
    setOpenForm(false);
  };

  useEffect(() => {
    getFleet();
  }, []);

  const getFleet = () => {
    Axios.get("http://localhost:9001/flight/fleet/all").then((response) => {
      console.log(response);
      setFleet(response.data);
    });
  };
  const handleClickOpen = (idx) => {
    // console.log(event.idx)
    let index = fleet[idx].id;
    console.log(index);

    Axios.get("http://localhost:9001/flight/fleet/find/id/" + index).then(
      (response) => {
        setUFleet(response.data);
        console.log(response);
      }
    );
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleDelete = async () => {
    console.log(uFleet);
    uFleet["status"] = "inactive";

    await Axios.put(
      "http://localhost:9001/flight/fleet/update",
      uFleet
    ).then((response) => {
      console.log(response);
      setOpen(false);
    });

    getFleet();
  };
  return (
    <div>
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table
          stickyHeader
          aria-label="sticky table"
          onCellClick={(rowNumber, columnId) =>
            console.log(rowNumber, columnId)
          }
        >
          <TableHead>
            <TableRow>
                <TableCell>Fleet Id</TableCell>
                <TableCell> Code</TableCell>
                <TableCell> Model</TableCell>
                <TableCell>Total Buisness Seats</TableCell>
                <TableCell>Total Economy Seats</TableCell>
                <TableCell>Total Premium Seats</TableCell>
                <TableCell> Status</TableCell>
              <TableCell> Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {fleet.map((fleet, idx) => {
              return (
                <TableRow hover role="checkbox" key={fleet.id}>
                  {" "}
                  {/*   tabIndex={-1}*/}
                    <TableCell>{fleet.id}</TableCell>
                    <TableCell>{fleet.code}</TableCell>
                    <TableCell>{fleet.model}</TableCell>
                    <TableCell>{fleet.totalBusinessSeats}</TableCell>
                    <TableCell>{fleet.totalEconomySeats}</TableCell>
                    <TableCell>{fleet.totalPremiumSeats}</TableCell>
                    <TableCell>{fleet.status}</TableCell>
                  <TableCell>
                    <Button
                      onClick={() => {
                        handleChangeEdit(idx);
                      }}
                    >
                      <EditIcon />
                    </Button>
                    
                    <Button
                      onClick={() => {
                        handleClickOpen(idx);
                      }}
                    >
                      {/* onClick={(tabIndex)=>{handleChangeDelete(idx)}} */}
                      {/* onClick= { (key)=>{handleClick(key)}} */}
                      <DeleteOutlineIcon />
                    </Button>
                   

                    {/* <Link to ={`/admin/Fleet/${Fleet.id}`}>
                      <DeleteOutlineIcon />
                      </Link> */}
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>

      
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={fleet.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  
    <Link 
    to="/admin/Fleet/add" > Add Fleet </Link>

    <div>
     <Dialog
     open={open}
     onClose={handleClose}
     // aria-labelledby="alert-dialog-title"
     // aria-describedby="alert-dialog-description"
   >
     <DialogTitle id="alert-dialog-title">
       {"Delete?"}
     </DialogTitle>
     <DialogContent>
       <DialogContentText id="alert-dialog-description">
         Are you want to delete this data.
       </DialogContentText>
     </DialogContent>
     <DialogActions>
       <Button onClick={handleClose} color="primary">
         Disagree
       </Button>
       <Button
         onClick={handleDelete}
         color="primary"
         autoFocus
       >
         Agree
       </Button>
     </DialogActions>
   </Dialog>
   </div>
   <div>
   <Dialog
    open={openForm}
    onClose={handleFormClose}
    // aria-labelledby="alert-dialog-title"
    // aria-describedby="alert-dialog-description"
  >
    <DialogTitle id="alert-dialog-title">
      {"Edit Form"}
    </DialogTitle>
    <DialogContent>
      <div className={classes.root}>
        <Grid container spacing={2} justify="center">
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <form>
                <div
                  style={{
                    marginLeft: "30px",
                    marginTop: "10px",
                  }}
                >
                  <TextField
                    variant="outlined"
                    label=" Fleet Id"
                    value={eFleet.id}
                    name="id"
                    // onChange={handleInputChange}
                    // style={{ width: 500 }}
                    // fullWidth
                  />
                  <TextField
                    variant="outlined"
                    label=" Code"
                    value={eFleet.code}
                    name="code"
                    onChange={handleInputChange}
                  />
                  <TextField
                    variant="outlined"
                    label=" Airport Model"
                    value={eFleet.model}
                    name="model"
                    onChange={handleInputChange}
                  />
                  <TextField
                    variant="outlined"
                    label="Total Buisness Seats"
                    value={eFleet.totalBusinessSeats}
                    name="totalBusinessSeats"
                    onChange={handleInputChange}
                  />
                  <TextField
                    variant="outlined"
                    label=" Total Economy Seats"
                    value={eFleet.totalEconomySeats}
                    name="totalEconomySeats"
                    onChange={handleInputChange}
                  />
                  <TextField
                    variant="outlined"
                    label=" Total Premium Seats"
                    value={eFleet.totalPremiumSeats}
                    name="totalPremiumSeats"
                    onChange={handleInputChange}
                  />
                  <FormControl component="fieldset">
                    <FormLabel component="legend">Status</FormLabel>
                    <RadioGroup aria-label="gender" name='status' value={eFleet.status} onChange={handleInputChange}>
                      <FormControlLabel value="active" control={<Radio />} label="Active" />
                      <FormControlLabel value="inactive" control={<Radio />} label="InActive" />
                    </RadioGroup>
                  </FormControl>
                  <div>
                    
                    {/* <Button onClick={handleFormClose}>
                    </Button> */}
                  </div>
                </div>
              </form>
            </Paper>
          </Grid>
        </Grid>
      </div>
      {/* <DialogContentText id="alert-dialog-description">
        Are you want to delete this data.
      </DialogContentText> */}
    </DialogContent>
    <DialogActions>
      <Button onClick={handleFormClose} color="primary">
        Disagree
      </Button>
      {/* <Button
        onClick={handleDelete}
        color="primary"
        autoFocus
      >
        Agree
      </Button> */}
       <Button
        variant="contained"
        type="submit"
        color="primary"
        onClick={handleClick}
        autoFocus
      >
        Add
      </Button>
    </DialogActions>
  </Dialog>
  </div>
   </div>
    );
    
  
};

export default FleetTable;
