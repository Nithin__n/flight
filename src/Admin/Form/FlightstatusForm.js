import React, { useState } from 'react'
import { FormControl, FormLabel, FormGroup, FormControlLabel, Paper, makeStyles, TextField, Grid, Button } from '@material-ui/core'
import Axios from 'axios'
import { Link } from 'react-router-dom'

const initialValues={
    id : 0,
    airportName : '',
    code: '',
    country : '',
    name: '',
    status: 'active'

}
const useStyles= makeStyles((theme)=>({

    root :{
        display : "flex",
        flexGrow: 1,
        flexWrap: 'wrap',
        '& .MuiInputBase-root ' :{
            width : '75ch',
            margin : theme.spacing(0.5),
             
            }
       
    },
    textField :{
        width : '50ch'
    
    },
    paper:{
        elevation :3,
        margin: theme.spacing(10),
        marginTop :theme.spacing(5),
      width: theme.spacing(90),
      height: theme.spacing(50),
    }
 // 
    // paperContent : {
        
    //     margin : theme.spacing(5),
    //     padding : theme.spacing(3),
    //     marginLeft: theme.spacing(1),
    // marginRight: theme.spacing(1),
    // width: '25ch',
    // }

}))
const FlightstatusForm=()=>{

    const classes= useStyles();
    const [values, setValues] = useState(initialValues)

    const handleInputChange =(event)=>{
        // const [name, value] = event.target
        setValues({
            ...values, [event.target.name]:  event.target.value
        })

    }


    const handleClick =()=>{
        Axios.post('http://localhost:9001/flight/status', values )
            .then((response)=>{
                console.log(response);
            })
    }
    return(
        
        <div className= {classes.root} >
        
           
                <Grid container spacing={2}
                justify ="center"
                 >
                    <Grid 
                    item xs ={12}   >
                    <Paper className= {classes.paper}>
                    <form>
                        <div style ={ {marginLeft:'30px', marginTop :'10px'}}>
                        <TextField  
                       
                        variant= "outlined"
                        label =" Flightstatus Id"
                        value= {values.id}
                        name= "id"
                        onChange ={handleInputChange}
                        // style={{ width: 500 }}
                        // fullWidth

                        />
                        <TextField  
                        
                        variant= "outlined"
                        label =" Remaining Economy Seats"
                        value= {values.remainingEconomySeats}
                        name= "remainingEconomySeats"
                        onChange = {handleInputChange}
                        />
                        <TextField  
                        variant= "outlined"
                        label ="Remaining Premiun Seats"
                        value= {values.remainingPremiunSeats}
                        name= "remainingPremiunSeats"
                        onChange = {handleInputChange}
                        />
                        <TextField  
                        variant= "outlined"
                        label =" Remaining Business Seats"
                        value= {values.remainingBusinessSeats}
                        name= "remainingBusinessSeats"
                        onChange = {handleInputChange}
                        />
                        <div>
                        <Button 
                        variant = "contained"
                        type = 'submit'
                        color = "primary"
                        onClick ={handleClick}>Add</Button>
                        <Button onClick ={handleClick}><Link to ="/admin/Flightstatus">Cancel</Link></Button>
                        </div>
                        </div>
                        </form>
                       </Paper> 
                    </Grid>
                </Grid>
        
        </div>
    )
}
export default FlightstatusForm