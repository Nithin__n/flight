import React, { useState } from 'react'
import { FormControl, FormLabel, FormGroup, FormControlLabel, Paper, makeStyles, TextField, Grid, Button } from '@material-ui/core'
import Axios from 'axios'
import { Link } from 'react-router-dom'

const initialValues={
    id : 0,
    airportName : '',
    code: '',
    country : '',
    name: '',
    status: 'active'

}
const useStyles= makeStyles((theme)=>({

    root :{
        display : "flex",
        flexGrow: 1,
        flexWrap: 'wrap',
        '& .MuiInputBase-root ' :{
            width : '75ch',
            margin : theme.spacing(0.5),
             
            }
       
    },
    textField :{
        width : '50ch'
    
    },
    paper:{
        elevation :3,
        margin: theme.spacing(10),
        marginTop :theme.spacing(5),
      width: theme.spacing(90),
      height: theme.spacing(50),
    }
 // 
    // paperContent : {
        
    //     margin : theme.spacing(5),
    //     padding : theme.spacing(3),
    //     marginLeft: theme.spacing(1),
    // marginRight: theme.spacing(1),
    // width: '25ch',
    // }

}))
const FareForm=()=>{

    const classes= useStyles();
    const [values, setValues] = useState(initialValues)

    const handleInputChange =(event)=>{
        // const [name, value] = event.target
        setValues({
            ...values, [event.target.name]:  event.target.value
        })

    }


    const handleClick =()=>{
        Axios.post('http://localhost:9001/flight/fare', values )
            .then((response)=>{
                console.log(response);
            })
    }
    return(
        
        <div className= {classes.root} >
        
           
                <Grid container spacing={2}
                justify ="center"
                 >
                    <Grid 
                    item xs ={12}   >
                    <Paper className= {classes.paper}>
                    <form>
                        <div style ={ {marginLeft:'30px', marginTop :'10px'}}>
                        <TextField  
                       
                        variant= "outlined"
                        label =" Fare Id"
                        value= {values.id}
                        name= "id"
                        onChange ={handleInputChange}
                        // style={{ width: 500 }}
                        // fullWidth

                        />
                        <TextField  
                        
                        variant= "outlined"
                        label =" Economy Fare"
                        value= {values.economyFare}
                        name= "economyFare"
                        onChange = {handleInputChange}
                        />
                        <TextField  
                        variant= "outlined"
                        label =" Premium Fare"
                        value= {values.premiumfare}
                        name= "premiumfare"
                        onChange = {handleInputChange}
                        />
                        <TextField  
                        variant= "outlined"
                        label =" Buisness Fare"
                        value= {values.businessFare}
                        name= "businessFare"
                        onChange = {handleInputChange}
                        />
                        <div>
                        <Button 
                        variant = "contained"
                        type = 'submit'
                        color = "primary"
                        onClick ={handleClick}>Add</Button>
                        <Button onClick ={handleClick}><Link to ="/admin/fare">Cancel</Link></Button>
                        </div>
                        </div>
                        </form>
                       </Paper> 
                    </Grid>
                </Grid>
        
        </div>
    )
}
export default FareForm