import React, { useState } from 'react'
import { FormControl, FormLabel, FormGroup, FormControlLabel, Paper, makeStyles, TextField, Grid, Button } from '@material-ui/core'
import Axios from 'axios'
import { Link } from 'react-router-dom'

const initialValues={
    id : 0,
    airportName : '',
    code: '',
    country : '',
    name: '',
    status: 'active'

}
const useStyles= makeStyles((theme)=>({

    root :{
        display : "flex",
        flexGrow: 1,
        flexWrap: 'wrap',
        '& .MuiInputBase-root ' :{
            width : '75ch',
            margin : theme.spacing(0.5),
             
            }
       
    },
    textField :{
        width : '50ch'
    
    },
    paper:{
        elevation :3,
        margin: theme.spacing(10),
        marginTop :theme.spacing(5),
      width: theme.spacing(90),
      height: theme.spacing(50),
    }
 // 
    // paperContent : {
        
    //     margin : theme.spacing(5),
    //     padding : theme.spacing(3),
    //     marginLeft: theme.spacing(1),
    // marginRight: theme.spacing(1),
    // width: '25ch',
    // }

}))
const FlightForm=()=>{

    const classes= useStyles();
    const [values, setValues] = useState(initialValues)

    const handleInputChange =(event)=>{
        // const [name, value] = event.target
        setValues({
            ...values, [event.target.name]:  event.target.value
        })

    }


    const handleClick =()=>{
        Axios.post('http://localhost:9001/flight', values )
            .then((response)=>{
                console.log(response);
            })
    }
    return(
        
        <div className= {classes.root} >
        
           
                <Grid container spacing={2}
                justify ="center"
                 >
                    <Grid 
                    item xs ={12}   >
                    <Paper className= {classes.paper}>
                    <form>
                        <div style ={ {marginLeft:'30px', marginTop :'10px'}}>
                        <TextField  
                       
                        variant= "outlined"
                        label =" Flight Id"
                        value= {values.flightId}
                        name= "flightId"
                        onChange ={handleInputChange}
                        // style={{ width: 500 }}
                        // fullWidth

                        />
                        <TextField  
                        
                        variant= "outlined"
                        label ="Arrival Time"
                        value= {values.arrivalTime}
                        name= "arrivalTime"
                        onChange = {handleInputChange}
                        />
                        <TextField  
                        
                        variant= "outlined"
                        label ="Departure Time"
                        value= {values.departureTime}
                        name= "departureTime"
                        onChange = {handleInputChange}
                        />
                        <TextField  
                        variant= "outlined"
                        label ="Date"
                        value= {values.date}
                        name= "date"
                        onChange = {handleInputChange}
                        />
                        <TextField  
                        variant= "outlined"
                        label ="Arrival LocationId"
                        value= {values.arrivalLocationId}
                        name= "arrivalLocationId"
                        onChange = {handleInputChange}
                        />
                        <TextField  
                        variant= "outlined"
                        label ="Departure LocationId"
                        value= {values.departureLocationId}
                        name= "departureLocationId"
                        onChange = {handleInputChange}
                        />
                        <TextField  
                        variant= "outlined"
                        label ="Fleet Id"
                        value= {values.fleetId}
                        name= "fleetId"
                        onChange = {handleInputChange}
                        />
                        <TextField  
                        variant= "outlined"
                        label ="Fare Id"
                        value= {values.fareId}
                        name= "fareId"
                        onChange = {handleInputChange}
                        />
                        <TextField  
                        variant= "outlined"
                        label ="flight StatusId"
                        value= {values.flightStatusId}
                        name= "flightStatusId"
                        onChange = {handleInputChange}
                        />
                        <div>
                        <Button 
                        variant = "contained"
                        type = 'submit'
                        color = "primary"
                        onClick ={handleClick}>Add</Button>
                        <Button onClick ={handleClick}><Link to ="/admin/Flight">Cancel</Link></Button>
                        </div>
                        </div>
                        </form>
                       </Paper> 
                    </Grid>
                </Grid>
        
        </div>
    )
}
export default FlightForm