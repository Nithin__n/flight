import React from 'react';
import { Button } from '@material-ui/core';
import { Link } from 'react-router-dom';

const AdminDashboard = () => {

    // const preventDefault=(event)=>{event.preventDefault()};
    return (
        <div>
       <Link to= "/admin/location">
           Admin_location
       </Link>
       <br></br>
       <Link to='/admin/fleet'>
           Admin_fleet
       </Link>
       <br></br>
       <Link to='/admin/fare'>
           Admin_fare
       </Link>
       <br></br>
       <Link to='/admin/flightstatus'>
           Admin_flight_status
       </Link>
       <br></br>
       <Link to='/admin/flight'>
           Admin_flight
       </Link>
       </div>
    );
};


export default AdminDashboard;

