import React, { useState, useEffect } from "react";
import Axios from "axios";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import FlightstatusForm from "./Form/FlightstatusForm";
import { Link } from "react-router-dom";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import {  FormLabel, FormGroup, FormControlLabel, TextField, Grid } from '@material-ui/core'
const useStyles = makeStyles({
  root: {
    width: "100%",
    //   alignItems : 'center',
    //   justifyContent : 'center'
  },
  container: {
    maxHeight: 440,
  },
});

const FlightstatusTable = () => {
  const initialValues={
    id : 0,
    airportName : '',
    code: '',
    country : '',
    name: '',
    status: 'active'

}
  const classes = useStyles();
  const [flightstatus, setFlightstatus] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(4);
  const [uFlightstatus, setUFlightstatus] = useState({});
  const [open, setOpen] = useState(false);
  const [eFlightstatus, setEFlightstatus] = useState(initialValues);
  // const [values, setValues] = useState()
  const [openForm, setOpenForm] = useState(false);
  const handleInputChange =(event)=>{
    // const [name, value] = event.target
    setEFlightstatus({
        ...eFlightstatus, [event.target.name]:  event.target.value
    })

}


const handleClick =async()=>{
  console.log(eFlightstatus)
     await Axios.put('http://localhost:9001/flight/status/update', eFlightstatus )
        .then((response)=>{
            console.log(response);
            setOpenForm(false)
        })

        getFlightstatus();

}
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  // const handleClick = (id) => {
  //   console.log(id);
  // };
  const handleChangeEdit = async (idx) => {
    let index = flightstatus[idx].id;
    console.log(index);

    await Axios.get(
      "http://localhost:9001/flight/status/find/id/" + index
    ).then((response) => {
      setEFlightstatus(response.data);
      console.log(response);
    });
    setOpenForm(true)
  };
  const handleFormClose = () => {
    setOpenForm(false);
  };

  useEffect(() => {
    getFlightstatus();
  }, []);

  const getFlightstatus = () => {
    Axios.get("http://localhost:9001/flight/status/all").then((response) => {
      console.log(response);
      setFlightstatus(response.data);
    });
  };
  const handleClickOpen = (idx) => {
    // console.log(event.idx)
    let index = flightstatus[idx].id;
    console.log(index);

    Axios.get("http://localhost:9001/flight/status/find/id/" + index).then(
      (response) => {
        setUFlightstatus(response.data);
        console.log(response);
      }
    );
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleDelete = async () => {
    console.log(uFlightstatus);
    uFlightstatus["status"] = "inactive";

    await Axios.put(
      "http://localhost:9001/flight/status/update",
      uFlightstatus
    ).then((response) => {
      console.log(response);
      setOpen(false);
    });

    getFlightstatus();
  };
  return (
    <div>
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table
          stickyHeader
          aria-label="sticky table"
          onCellClick={(rowNumber, columnId) =>
            console.log(rowNumber, columnId)
          }
        >
          <TableHead>
            <TableRow>
                <TableCell>Flightstatus Id</TableCell>
                <TableCell>Remaining Economy Seats </TableCell>
                <TableCell>Remaining Premiun Seats</TableCell>
                <TableCell>Remaining Business Seats</TableCell>
                <TableCell> Status</TableCell>
              <TableCell> Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {flightstatus.map((flightstatus, idx) => {
              return (
                <TableRow hover role="checkbox" key={flightstatus.id}>
                  {" "}
                  {/*   tabIndex={-1}*/}
                    <TableCell>{flightstatus.id}</TableCell>
                    <TableCell>{flightstatus.remainingEconomySeats}</TableCell>
                    <TableCell>{flightstatus.remainingPremiunSeats}</TableCell>
                    <TableCell>{flightstatus.remainingBusinessSeats}</TableCell>
                    <TableCell>{flightstatus.status}</TableCell>
                  <TableCell>
                    <Button
                      onClick={() => {
                        handleChangeEdit(idx);
                      }}
                    >
                      <EditIcon />
                    </Button>
                    
                    <Button
                      onClick={() => {
                        handleClickOpen(idx);
                      }}
                    >
                      {/* onClick={(tabIndex)=>{handleChangeDelete(idx)}} */}
                      {/* onClick= { (key)=>{handleClick(key)}} */}
                      <DeleteOutlineIcon />
                    </Button>
                   

                    {/* <Link to ={`/admin/Flightstatus/${Flightstatus.id}`}>
                      <DeleteOutlineIcon />
                      </Link> */}
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>

      
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={flightstatus.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  
    <Link 
    to="/admin/Flightstatus/add" > Add Flightstatus </Link>

    <div>
     <Dialog
     open={open}
     onClose={handleClose}
     // aria-labelledby="alert-dialog-title"
     // aria-describedby="alert-dialog-description"
   >
     <DialogTitle id="alert-dialog-title">
       {"Delete?"}
     </DialogTitle>
     <DialogContent>
       <DialogContentText id="alert-dialog-description">
         Are you want to delete this data.
       </DialogContentText>
     </DialogContent>
     <DialogActions>
       <Button onClick={handleClose} color="primary">
         Disagree
       </Button>
       <Button
         onClick={handleDelete}
         color="primary"
         autoFocus
       >
         Agree
       </Button>
     </DialogActions>
   </Dialog>
   </div>
   <div>
   <Dialog
    open={openForm}
    onClose={handleFormClose}
    // aria-labelledby="alert-dialog-title"
    // aria-describedby="alert-dialog-description"
  >
    <DialogTitle id="alert-dialog-title">
      {"Edit Form"}
    </DialogTitle>
    <DialogContent>
      <div className={classes.root}>
        <Grid container spacing={2} justify="center">
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <form>
                <div
                  style={{
                    marginLeft: "30px",
                    marginTop: "10px",
                  }}
                >
                  <TextField
                    variant="outlined"
                    label=" Flightstatus Id"
                    value={eFlightstatus.id}
                    name="id"
                    // onChange={handleInputChange}
                    // style={{ width: 500 }}
                    // fullWidth
                  />
                  <TextField
                    variant="outlined"
                    label=" Remaining Economy Seats"
                    value={eFlightstatus.remainingEconomySeats}
                    name="remainingEconomySeats"
                    onChange={handleInputChange}
                  />
                  <TextField
                    variant="outlined"
                    label="Remaining Premiun Seats"
                    value={eFlightstatus.remainingPremiunSeats}
                    name="remainingPremiunSeats"
                    onChange={handleInputChange}
                  />
                  <TextField
                    variant="outlined"
                    label="Remaining Business Seats"
                    value={eFlightstatus.remainingBusinessSeats}
                    name="remainingBusinessSeats"
                    onChange={handleInputChange}
                  />
                  <FormControl component="fieldset">
                    <FormLabel component="legend">Status</FormLabel>
                    <RadioGroup aria-label="gender" name='status' value={eFlightstatus.status} onChange={handleInputChange}>
                      <FormControlLabel value="active" control={<Radio />} label="Active" />
                      <FormControlLabel value="inactive" control={<Radio />} label="InActive" />
                    </RadioGroup>
                  </FormControl>
                  <div>
                    
                    {/* <Button onClick={handleFormClose}>
                    </Button> */}
                  </div>
                </div>
              </form>
            </Paper>
          </Grid>
        </Grid>
      </div>
      {/* <DialogContentText id="alert-dialog-description">
        Are you want to delete this data.
      </DialogContentText> */}
    </DialogContent>
    <DialogActions>
      <Button onClick={handleFormClose} color="primary">
        Disagree
      </Button>
      {/* <Button
        onClick={handleDelete}
        color="primary"
        autoFocus
      >
        Agree
      </Button> */}
       <Button
        variant="contained"
        type="submit"
        color="primary"
        onClick={handleClick}
        autoFocus
      >
        Add
      </Button>
    </DialogActions>
  </Dialog>
  </div>
   </div>
    );
    
  
};

export default FlightstatusTable;
