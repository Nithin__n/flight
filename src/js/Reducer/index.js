
import {ADD_USER, LOGOUT} from '../Constant/index'

const initialState = {
    user: []
  };
  
  function rootReducer(state = initialState, action) {
      if(action.type == ADD_USER){
        return Object.assign({}, state, {
            user: state.user.concat(action.payload)
          });
      }else if(action.type == LOGOUT){

        state = initialState;
      }
    return state;
  };
  
  export default rootReducer;