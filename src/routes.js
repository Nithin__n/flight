// import React from 'react';
// import { Navigate } from 'react-router-dom';
// import DashboardLayout from 'src/layouts/DashboardLayout';
// import MainLayout from 'src/layouts/MainLayout';
// import AccountView from 'src/views/account/AccountView';
// import CustomerListView from 'src/views/customer/CustomerListView';
// import DashboardView from 'src/views/reports/DashboardView';
// import LoginView from 'src/views/auth/LoginView';
// import NotFoundView from 'src/views/errors/NotFoundView';
// import RegisterView from 'src/views/auth/RegisterView';
// import SettingsView from 'src/views/settings/SettingsView';
// import AdminDashBoard from './views/Admin';

// const routes = [
//   {
//     path: 'app',
//     element: <DashboardLayout />,
//     children: [
//       { path: 'account', element: <AccountView /> },
//       { path: 'customers', element: <CustomerListView /> },
//       { path: 'dashboard', element: <DashboardView /> },
//       { path: 'settings', element: <SettingsView /> },
//       { path: 'admin', element: <AdminDashBoard /> }
//     ]
//   },
//   {
//     path: '/',
//     element: <MainLayout />,
//     children: [
//       { path: 'login', element: <LoginView /> },
//       { path: 'register', element: <RegisterView /> },
//       // { path: '404', element: <NotFoundView /> },
//       { path: '/', element: <Navigate to="/app/dashboard" /> },
//       { path: '*', element: <Navigate to="/404" /> }
//     ]
//   }
// ];

// export default routes;

import React from 'react';
import { Navigate } from 'react-router-dom';
import DashboardLayout from 'src/layouts/DashboardLayout';
import MainLayout from 'src/layouts/MainLayout';
import AccountView from 'src/views/account/AccountView';
import DashboardView from 'src/views/reports/DashboardView';
import LoginView from 'src/views/auth/LoginView';
import NotFoundView from 'src/views/errors/NotFoundView';
import RegisterView from 'src/views/auth/RegisterView';
import AdminDashBoard from './views/Admin';
import LocationTable from './views/Admin/Tables/LocationTable'
import FareTable from './views/Admin/Tables/FareTable'
import FleetTable from './views/Admin/Tables/FleetTable'
import FlightStatusTable from './views/Admin/Tables/FlightStatusTable'
import FlightTable from './views/Admin/Tables/FlightTable'
import FareForm from './views/Admin/Form/FareForm'
import FleetForm from './views/Admin/Form/FleetForm'
import FlightForm from './views/Admin/Form/FlightForm'
import FlightstatusForm from './views/Admin/Form/FlightstatusForm'
import LocationForm from './views/Admin/Form/LocationForm'
import Flights from './views/reports/DashboardView/Flights';
import AddBooking from './views/reports/DashboardView/Booking/AddBooking'
import Checkout from './views/reports/DashboardView/Booking/Checkout';
import ForgotPassword from './views/auth/ForgotPassword';
// import SignUp from './views/Login/SignUp'
// import SignIn from './views/Login/SignIn'

const routes = [
  {
    path: 'app',
    element: <DashboardLayout />,
    children: [
      { path: 'account', element: <AccountView /> },
      { path: 'dashboard', element: <DashboardView /> },
      { path: 'admin', element: <AdminDashBoard /> },
      { path: 'admin/location', element: <LocationTable /> },
      { path: 'admin/fare', element: <FareTable /> },
      { path: 'admin/status', element: <FlightStatusTable /> },
      { path: 'admin/flight', element: <FlightTable /> },
      { path: 'admin/fleet', element: <FleetTable /> },
      { path: 'admin/location/add', element: <LocationForm /> },
      { path: 'admin/fare/add', element: <FareForm /> },
      { path: 'admin/status/add', element: <FlightstatusForm /> },
      { path: 'admin/flight/add', element: <FlightForm /> },
      { path: 'admin/fleet/add', element: <FleetForm /> },
      {path:'/checkout/:id', elemet:<Checkout />},
       {path :'dashboard/flights/:date/:arrival/:departure/:seatType/:way/:rDate/:rseatType' , element:<Flights />},
      {path:'dashboard/AddBooking/:ticketsNo/:flightId/:arrival/:departure/:date/:seatType/:way/:rDate/:rseatType/:rflightId', element:<Checkout />}
    ]
  },
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: 'login', element: <LoginView /> },
      { path: 'register', element: <RegisterView /> },
      { path: 'reset', element: <ForgotPassword /> },
      // { path: '404', element: <NotFoundView /> },
       { path: '/', element: <Navigate to="/login" /> },
      // { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];

export default routes;
