import React from 'react';

const Logo = (props) => {
  return (
    <div>
    <img
      alt="Logo"
      src="/static/logo.svg"
      {...props}
    />
    </div>
  );
};

export default Logo;
