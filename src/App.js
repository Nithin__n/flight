import 'react-perfect-scrollbar/dist/css/styles.css';
import React from 'react';
import { useRoutes, BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core';
import GlobalStyles from 'src/components/GlobalStyles';
import 'src/mixins/chartjs';
import theme from 'src/theme';
import routes from 'src/routes';
import LoginView from 'src/views/auth/LoginView';
import DashboardView from 'src/views/reports/DashboardView';
import {Route, Router } from 'react-router';
import DashboardLayout from 'src/layouts/DashboardLayout';
const App = () => {
  const routing = useRoutes(routes);

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      {routing}
    </ThemeProvider>
  );
};

export default App;
