import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField,
  makeStyles
} from '@material-ui/core';
import Axios from 'axios';
import {connect} from 'react-redux'
import {useAlert} from 'react-alert'
const useStyles = makeStyles(({
  root: {}
}));

const Password = ({ className,user, ...rest }) => {
  const userId= user[0].id;
  const alert=useAlert()
  const classes = useStyles();
  const [values, setValues] = useState({
    password: '',
    confirm: ''
  });

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };
  const handleSubmit=(event)=>{
    Axios.put('http://localhost:8765/user-service/user/password', {password:values.password,id:userId })
    .then((res)=>{
      setValues({
        password: '',
        confirm: ''
      })
      alert.show('Password Updated Successfully')
          console.log(res)
    }).catch((err)=>{
      console.log(err)
    })

  }

  return (
    <form
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Card>
        <CardHeader
          subheader="Update password"
          title="Password"
        />
        <Divider />
        <CardContent>
          <TextField
            fullWidth
            label="Password"
            margin="normal"
            name="password"
            onChange={handleChange}
            type="password"
            value={values.password}
            variant="outlined"
          />
          <TextField
            fullWidth
            label="Confirm password"
            margin="normal"
            name="confirm"
            onChange={handleChange}
            type="password"
            value={values.confirm}
            variant="outlined"
          />
        </CardContent>
        <Divider />
        <Box
          display="flex"
          justifyContent="flex-end"
          p={2}
        >
          <Button
            color="primary"
            variant="contained"
            onClick={handleSubmit}
          >
            Update
          </Button>
        </Box>
      </Card>
    </form>
  );
};

const mapStateToProps= (state)=>{
  return {user: state.user}

}

export default connect(mapStateToProps)(Password)