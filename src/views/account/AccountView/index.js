import React from 'react';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import Password from './Password';
import ProfileDetails from './ProfileDetails';
import {connect} from 'react-redux'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const UserView = (props) => {
  const classes = useStyles();
  console.log(props)

  return (
    <Page
      className={classes.root}
      title="Settings"
    >
      <Container maxWidth="lg">
        <h3> {props.user[0].userName} your user Id is  :-- {props.user[0].id}</h3>
        <ProfileDetails />
        <Box mt={3}>
          <Password />
        </Box>
      </Container>
    </Page>
  );
};

const mapStateToProps= (state)=>{
  return {user: state.user}

}

const SettingsView= connect(mapStateToProps)(UserView)

export default SettingsView;






// import React from 'react';
// import {
//   Container,
//   Grid,
//   makeStyles
// } from '@material-ui/core';
// import Page from 'src/components/Page';
// // import Profile from './Profile';
// import ProfileDetails from './ProfileDetails';

// const useStyles = makeStyles((theme) => ({
//   root: {
//     backgroundColor: theme.palette.background.dark,
//     minHeight: '100%',
//     paddingBottom: theme.spacing(3),
//     paddingTop: theme.spacing(3)
//   }
// }));

// const Account = () => {
//   const classes = useStyles();

//   return (
//     <Page
//       className={classes.root}
//       title="Account"
//     >
//       <Container maxWidth="lg">
//         <Grid
//           container
//           spacing={3}
//         >
//           <Grid
//             item
//             lg={4}
//             md={6}
//             xs={12}
//           >
//             {/* <Profile /> */}
//           </Grid>
//           <Grid
//             item
//             lg={8}
//             md={6}
//             xs={12}
//           >
//             <ProfileDetails />
//           </Grid>
//         </Grid>
//       </Container>
//     </Page>
//   );
// };

// export default Account;
