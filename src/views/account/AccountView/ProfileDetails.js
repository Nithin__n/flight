import React, { useState,useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  makeStyles
} from '@material-ui/core';
import {connect} from 'react-redux'
import Axios from 'axios'
import {useAlert} from 'react-alert'
const useStyles = makeStyles(() => ({
  root: {}
}));

const UserDetails = (props) => {
const alert=useAlert();
  const userId=props.user[0].id;
  const classes = useStyles();
  const [values, setValues] = useState({
    id: userId,
    userName: "",
    password: "",
    firstName: "",
    lastName: "",
    contact: {
        address: {
            type: "",
            addressLine: "",
            zipCode: 0,
            city: "",
            state: "",
            country: ""
        },
        email: "",
        mobileNo: ""
    }
  });
  
  useEffect(()=>{
    getUserDetails();
  },[])
  const getUserDetails=()=>{
    Axios.get('http://localhost:8765/user-service/user/'+ userId)
    .then((response)=>{
      setValues(response.data)
    }).catch((err)=>{
      console.log(err)
    })
  }

  const handleSubmit=async (event)=>{
    event.preventDefault();
    console.log(values)
   await Axios.put('http://localhost:8765/user-service/user', values)
    .then((response)=>{
      alert.show('User Details Updated SucessFully')
      console.log(response)
      getUserDetails();
    })
    .catch(
        (error)=>{
            console.log(error.response);
        }
    );

  }
  const handleChange=(event)=>{
    setValues({...values, [event.target.name]: event.target.value })
  }
const handleContact =(event)=>{

  setValues({...values, contact:{...values.contact, [event.target.name]:event.target.value}} )
}
const handleAddress=(event)=>{
  setValues({...values, contact:{...values.contact, address :{...values.contact.address, [event.target.name]:event.target.value}}} )
}
  return (
    <form>
      <Card>
        <CardHeader
          title="Profile"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
              fullWidth
                variant= "outlined"
                label ="First Name"
                value= {values.firstName}
                name= "firstName"
                onChange ={handleChange}
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Last name"
                name="lastName"
                onChange={handleChange}
                required
                value={values.lastName}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Email Address"
                name="email"
                onChange={handleContact}
                required
                value={values.contact.email}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Phone Number"
                name="mobileNo"
                onChange={handleContact}
                // type="number"
                value={values.contact.mobileNo}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Country"
                name="country"
                onChange={handleAddress}
                required
                value={values.contact.address.country}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Street"
                name="addressLine"
                onChange={handleAddress}
                required
                value={values.contact.address.addressLine}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="City"
                name="city"
                onChange={handleAddress}
                required
                value={values.contact.address.city}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="State"
                name="state"
                value={values.contact.address.state}
                onChange={handleAddress}
                required
                
                variant="outlined"
              />
            </Grid>
           
            
            
            
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Zip Code"
                name="zipCode"
                onChange={handleAddress}
                required
                value={values.contact.address.zipCode}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <Box
          display="flex"
          justifyContent="flex-end"
          p={2}
        >
          <Button
            color="primary"
            variant="contained"
            onClick={handleSubmit}
          >
            Save details
          </Button>
        </Box>
      </Card>
    </form>
  );
};

const mapStateToProps= (state)=>{
  return {user: state.user}

}

const ProfileDetails= connect(mapStateToProps)(UserDetails)
export default ProfileDetails;
