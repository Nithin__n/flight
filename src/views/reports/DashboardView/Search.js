
import Axios from 'axios'
import  React, { useState, useEffect}  from 'react'
import {  useNavigate } from 'react-router-dom';
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import { Link } from "react-router-dom";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import {  FormLabel, FormGroup, FormControlLabel, TextField, Grid } from '@material-ui/core'
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import { Box, Card, CardContent, CardHeader, Divider } from '@material-ui/core';
const useStyles = makeStyles({
    root: {
      width: "100%",
      //   alignItems : 'center',
      //   justifyContent : 'center'
    },
    container: {
      maxHeight: 440,
    },
  });

const Search=()=>{
    const classes= useStyles();
    const navigate = useNavigate();
    const [values , setValues]=useState({
            date: '',
            arrival: '',
            departure:'',
            seatType:'Economy',
            way:"oneWay",
            rDate:'0000-00-00',
            rseatType:'Economy',
            lList:[]
         })
    const handleChange=(event)=>{
        setValues({...values, [event.target.name] : event.target.value})
    }

    // const datalist=values.lList.map(loc=>(
    //     <option value={loc}></option>
    // ))

    const handleSubmit = event =>{
        if(values.date.length == 0 ||values.arrival.length==0 || values.departure.length==0 || values.seatType==0){
          alert("Please Enter all the Fields")
        }else{
        navigate('/app/dashboard/flights/'+ values.date + '/'+values.arrival + '/'+values.departure + '/'+values.seatType + '/'+values.way + '/'+values.rDate + '/'+values.rseatType + '/', { replace: true });
        }
        // this.props.history.push("/app/flights/"+this.state.date+"/"+this.state.arrival+"/"+this.state.departure+"/"+this.state.seatType+"/"+this.state.oneWay+"/"+this.state.rDate+"/"+this.state.rseatType)
    }

    useEffect(()=>{
        Axios.get('http://localhost:8010/search/getalllocations')
            .then(response=>{
                setValues({...values, [values.lList]:response.data})
            })
    },[])
    return (
      <Box width="100%">
      <form onSubmit={handleSubmit}>
        <Card style={{ width: '300' }}>
          <CardHeader title="Search" />
          <Divider />
          <CardContent>
            <Grid container spacing={0.5}>
              <Grid item xs={6} sm={3}>
              
              <TextField
              required
              type="date"
                    variant="outlined"
                    value={values.date}
                    name="date"
                    onChange={handleChange}
                    // style={{ width: 500 }}
                    // fullWidth
                  />
              </Grid>
              <Grid item xs={6} sm={3}>
              <TextField
              required
                    variant="outlined"
                    label=" From"
                    value={values.departure}
                    name="departure"
                    onChange={handleChange}
                  />
              </Grid>
              <Grid item xs={6} sm={3}>
              <TextField
              required
                    variant="outlined"
                    label=" To"
                    value={values.arrival}
                    name="arrival"
                    onChange={handleChange}
                  />
              </Grid>
              <Grid item xs={6} sm={3}>
              <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="age-native-simple">Type</InputLabel>
                        <Select
                        native
                        value={values.seatType}
                        onChange={handleChange}
                        inputProps={{
                            name: 'Class',
                            id: 'age-native-simple',
                        }}
                        >
                        <option value={'Economy'}>Economy</option>
                        <option value={'Business'}>Business</option>
                        <option value={'Premium'}>Premium</option>
                        </Select>
                  
                   </FormControl>
              </Grid>
              
            </Grid>
          </CardContent>
          
          <Box display="flex" justifyContent="flex-center" p={2}>
            <Button variant="contained" type="submit" color="primary"  onClick={handleSubmit}>
              Happy Easy Search
            </Button>
          </Box>
        </Card>
      </form>
    </Box>
       
      
                  
    )
}


export default Search;









// import Axios from 'axios'
// import  React, { useState, useEffect}  from 'react'
// import {  useNavigate } from 'react-router-dom';

// const Search=()=>{
//     const navigate = useNavigate();
//     const [values , setValues]=useState({
//             date: '',
//             arrival: '',
//             departure:'',
//             seatType:'Economy',
//             oneWay:true,
//             rDate:'0000-00-00',
//             rseatType:'Economy',
//             lList:[]
//          })

//          const search_card={
//             "background-color": "whitesmoke"
//              }
//            const style1={
//                "color": "azure",
//                "background-color":"#6600cc"
//            }
   
//            const style2={
//                "margin-top": "100px"
//            }

//     const handleChange=(event)=>{
//         setValues({...values, [event.target.name] : event.target.value})
//     }

//     const datalist=values.lList.map(loc=>(
//         <option value={loc}></option>
//     ))

//     const handleSubmit = event =>{
//         if(values.departure.length==0 || values.arrival.length==0 || values.date.length==0 || (!values.oneWay && values.rDate=="0000-00-00")){
//            alert('Please Enter All the Fields')
//         }
//     else{
//         navigate('/app/dashboard/flights/'+ values.date + '/'+values.arrival + '/'+values.departure + '/'+values.seatType + '/'+values.oneWay + '/'+values.rDate + '/'+values.rseatType + '/', { replace: true });
//         // this.props.history.push("/app/flights/"+this.state.date+"/"+this.state.arrival+"/"+this.state.departure+"/"+this.state.seatType+"/"+this.state.oneWay+"/"+this.state.rDate+"/"+this.state.rseatType)
//     }
//     }

//     useEffect(()=>{
//         Axios.get('http://localhost:8010/search/getalllocations')
//             .then(response=>{
//                 setValues({...values, [values.lList]:response.data})
//             })
//     })
//     return (
//     <div>
//         <div className="container" style={style2}>
//             <div className="card" style={search_card}>
//                 <h2 className="card-header" style={style1}>Search Flights</h2>
//                 <div className="card-body">
//                     <form>
//                         <div className="container">
//                             <div className="row">
//                                 <div className="offset-2 col-2">
//                                     <div class="form-check form-check-inline">
//                                             <input class="form-check-input" type="radio" name="one-way" value="true" defaultValue={values.oneWay} onChange={handleChange} defaultChecked/>
//                                         <label class="form-check-label" for="one-way">One-way</label>
//                                     </div>
//                                 </div>
//                                 <div className="col-2">
//                                     <div class="form-check form-check-inline">
//                                         <input class="form-check-input" type="radio" name="one-way" value="false" defaultValue={!values.oneWay}  onChange={handleChange} />
//                                         <label class="form-check-label" for="return">Round-Trip</label>
//                                         </div>
//                                 </div>

//                             </div>
//                             <br />
                    
                        
//                             <div className="row">
//                                 <div className="col-2">
//                                     <label>Departure:  </label>
//                                 </div>
//                                 <div className="col-4">
//                                     <input list="browsers" name="departure" onChange={handleChange} />
//                                 </div>
//                                 <div className="col-2">
//                                     <label>Arrival:  </label>
//                                 </div>
//                                 <div className="col-4">
//                                     <input list="browsers" name="arrival" onChange={handleChange} />
//                                     <datalist id="browsers">
//                                         {datalist}
//                                     </datalist>
//                                 </div>
//                             </div>

//                             <div className="row">
//                                 <div className="col-2">
//                                     <label>Depart On:  </label>
//                                 </div>
//                                 <div className="col-4">
//                                     <input type="date" name="date" onChange={handleChange} />
//                                 </div>
//                                 <div className="col-2">
//                                     <label>Seat Type:  </label>
//                                 </div>
//                                 <div className="col-4">
//                                     <select name="seatType" value={values.seatType} onChange={handleChange} >
//                                         <option value="Economy">Economy</option>
//                                         <option value="Business">Business</option>
//                                         <option value="Premium">Premium</option>
//                                     </select>
//                                 </div>
//                             </div>
//                             <br />



//                             {!values.oneWay && 
//                                 <div className="row">
//                                     <div className="col-2">
//                                         <label>Return On:  </label>
//                                     </div>
//                                     <div className="col-4">
//                                         <input type="date" name="rDate" onChange={handleChange} />
//                                     </div>
//                                     <div className="col-2">
//                                         <label>Seat Type (Return):  </label>
//                                     </div>
//                                     <div className="col-4">
//                                         <select name="rseatType" value={values.rseatType} onChange={handleChange} >
//                                             <option value="Economy">Economy</option>
//                                             <option value="Business">Business</option>
//                                             <option value="Premium">Premium</option>
//                                         </select>
//                                     </div>
//                                 </div>
//                                 }
                            
//                             {values.oneWay && <br />}
//                             <div className="row">
//                                 <div className="col-2">
//                                     {/* {this.state.error && <p id="error_warning">Please enter All the Fields</p>} */}
//                                 </div>
//                             <div className="col-10">
//                                 <button type="submit" className="btn-primary" onClick={handleSubmit}>Search</button>
//                             </div>
//                             </div>
//                         </div>
//                     </form>
//                 </div>
//             </div>
//         </div>
//     </div>
//     )
// }


// export default Search;