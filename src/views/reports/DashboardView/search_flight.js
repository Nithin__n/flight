import axios from 'axios'
import  React, { useEffect,useState }  from 'react'
import Axios from 'axios'

const Search2=(props)=>{

    const [values, setValues]=useState({
            date: '',
            arrival: '',
            departure:'',
            seatType:'Economy',
            oneWay:'',
            rDate:'0000-00-00',
            rseatType:'Economy',
            lList:[]
    })

    const datalist= values&& values.lList && values.lList.map(loc=>(
        <option value={loc}></option>
    ))

    const style1={
        "margin-top":"20px"
    }
    const style2={
        "margin-top":"10px"
    }
   const handleChange = event =>{
        setValues({...values, [event.target.name]:event.target.value})
    }

   const handleSubmit = event =>{
        event.preventDefault()
        if(values.departure.length==0 || values.arrival.length==0 || values.date.length==0 || (!values.oneWay && values.rDate=="0000-00-00")){
            alert('Please Enter All the Fields')
         }
    else{
     props.changeState(values.oneWay,values.arrival,values.departure,values.date,values.rDate,values.seatType,values.rseatType)
    }
    }

    useEffect(()=>{
        Axios.get('http://localhost:8010/search/getalllocations')
            .then(response=>{
            setValues({...values,
                lList:response.data
             })
            })

            setValues({
                arrival:props.values.arrival,
                departure:props.values.departure,
                oneWay:props.values.oneWay,
               
            })
    })

    return(
        <div style={style1}>
        <form>
             <div className="container">
                 <div className="row">
                     <div className="col-1">
                         <label>Departure:  </label>
                     </div>
                     <div className="col-2">
                         <input list="browsers" name="departure" placeholder={values.departure} onChange={handleChange} />
                      </div>

                     <div className="col-1 offset-1">
                         <label>Arrival:  </label>
                     </div>
                     <div className="col-2">
                         <input list="browsers" name="arrival" placeholder={values.arrival} onChange={handleChange} />
                         <datalist id="browsers">
                             {datalist}
                         </datalist>
                     </div>
                     <div className="col-2 offset-1">
                          <label>Depart On:  </label>
                     </div>
                     <div className="col-2">
                         <input type="date" name="date" placeholder={values.date} onChange={handleChange} />
                     </div>
                 </div>
                 <br />
                 <div className="row">
             
                     <div className="col-2">
                         <label>Seat Type:  </label>
                     </div>
                     <div className="col-2">
                         <select name="seatType" value={values.seatType} placeholder={values.seatType}  onChange={handleChange} >
                             <option value="Economy">Economy</option>
                             <option value="Business">Business</option>
                             <option value="Premium">Premium</option>
                         </select>
                     </div>
             
             
                     {!(values.oneWay=="true") && 
                         <div className="col-2">
                             <label> Seat Type (Return):  </label>
                         </div>
                      }
                     {!(values.oneWay=="true") &&
                         <div className="col-2">
                             <select name="rseatType" value={values.rseatType} onChange={handleChange} >
                                 <option value="Economy">Economy</option>
                                 <option value="Business">Business</option>
                                 <option value="Premium">Premium</option>
                             </select>
                         </div>
             
                     }
                     {!(values.oneWay=="true") && 
                         <div className="col-2">
                             <label> Return On:  </label>
                         </div>
                     }
                     
                     {!(values.oneWay=="true") &&
                         <div className="col-2">
                             <input type="date" name="rDate" onChange={handleChange} />
                         </div>
                     }
                 </div>
                 <div className="row" style={style2}>
                     <div className="col-11">
             
                     </div>
                     <div className="col-1">
                         <button type="submit" className="btn-primary" onClick={handleSubmit}>Search</button>
                     </div>
                 </div>

             </div>
         </form>
     </div>

    )
}

export default Search2;