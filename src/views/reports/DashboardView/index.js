import React from 'react';
import {
  Container,
  Grid,
  Box,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import { useParams } from 'react-router-dom';
import {connect} from 'react-redux'
import BookedTable from './BookedTables'
import Search from './Search'
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const UserDashboard = (props) => {
console.log(props.user.length)
const length=props.user.length
console.log(props.user[length-1])
  const index= useParams('id');
  const classes = useStyles();

  return (
    <Page
      className={classes.root}
      title="Dashboard"
    >
      <Container maxWidth="lg">
        
        <Search />
        <Box mt={3} color="primary">
          <BookedTable />
        </Box>
      </Container>
    </Page>
  );
};

const mapStateToProps= (state)=>{
  return {user: state.user}

}

const Dashboard= connect(mapStateToProps)(UserDashboard)
export default Dashboard
