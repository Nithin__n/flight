import React, { useState,useEffect } from 'react'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button'; 
import MenuItem from '@material-ui/core/MenuItem';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Axios from 'axios'
import { Link , useParams } from 'react-router-dom';
//import {btn} from './Checkout';
const disable=false;
const initialValues={
  personId: 0,
  expiryDate: '',
  cardNo:'',
  nameOnCard:'',
  cvv:'',
}
let confirmStatus=false
const PaymentForm=(props)=>{
  const {ticketsNo, flightId, seatType}=props;
  console.log(props)
  const [values, setValues] = useState(initialValues)
  const [amount,setAmount] = useState(0)
  const [disable, setDisable] = useState(false);
  const [count , setCount] = useState(0);
  const {bookingId} = useParams();
  const[isValid, setIsValid]= useState(true)
  const [status, setStatus]= useState(false)
  const handleInputChange =(event)=>{

    if(event.target.name=="cvv" ){
      if(values.cvv.length  > 3) {
        setIsValid(true)
       }
       else{
         setIsValid(false)
       }
    }
      setValues({
          ...values, [event.target.name]:  event.target.value
      })

      

  }
  
  

  useEffect(( )=>{
getAmount()
  }, [])
  const getAmount=()=>{
    Axios.get('http://localhost:8765/admin-service/flight/find/id/'+flightId )
    .then((response)=>{
        console.log(response.data.fareId);
        const fareId= response.data.fareId? response.data.fareId: 0;
        Axios.get('http://localhost:8765/admin-service/flight/fare/find/id/'+fareId)
        .then((res)=>{
            console.log(res.data)
            if(seatType == "Business"){
              setAmount(res.data.businessFare * ticketsNo);
            }else if(seatType== "Premium"){
              setAmount(res.data.premiumfare * ticketsNo);
              
            }else{
              setAmount(res.data.economyFare * ticketsNo);
            }
            })
           
        })
      
    }
  

const handleClick =async(event)=>{
  props.confirmStatus(status)
  setStatus(false)
  confirmStatus= status;
  event.preventDefault()
  console.log(values)
  const temp ={
     paymentId:parseInt(values.paymentId),
      nameOnCard:values.nameOnCard,
      cvv: values.cvv,
      expiryDate: values.expiryDate,
      cardNo: values.cardNo
      }

  console.log(temp)
  Axios.post('http://localhost:8765/flight-search-booking-service/payments/addpayment', temp )
      .then((response)=>{
          console.log(response);
          setCount(count+1);
          if(count>=0)
          {
            setDisable(true);
            //btn=true;
          }
      })
}

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Payment Gateway
       </Typography>
      <Typography variant="h6" gutterBottom>
       Pay  Rs: {amount}
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            name="nameOnCard"
            label="Card Holder's Name"
            value={values.nameOnCard}
            fullWidth
            onChange={handleInputChange}
            
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="cardNo"
            name="cardNo"
            label="Card Number"
            value={values.cardNo}
            onChange ={handleInputChange}
            fullWidth

          />
          </Grid>
          <Grid item xs={12} sm={6}>
          <TextField
          // <TextField
          // id="date"
          // label="Birthday"
          // type="date"
          // defaultValue="2017-05-24"
          // className={classes.textField}
          // InputLabelProps={{
            required
            id="expiryDate"
            type="date"
            name="expiryDate"
           helperText="Expiry Date"
            value={values.expiryDate}
            onChange ={handleInputChange}
            fullWidth
/>
          </Grid>

        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="cvv"
            name="cvv"
            label="CVV"
            helperText="Last 3 digits of the Signature strip"
            value={values.cvv}
            onChange ={handleInputChange}
            fullWidth

          />
          </Grid>
          <Button 
disabled={isValid}
variant = "contained"
type = 'submit'
color = "primary"
onClick ={handleClick} set>Pay<Link to ="/payment"></Link>
</Button>


      </Grid>
    </React.Fragment>
  )
      }
  export  {PaymentForm, confirmStatus};
  
