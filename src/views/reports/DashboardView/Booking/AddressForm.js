import React, { useState } from 'react'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button'; 
import MenuItem from '@material-ui/core/MenuItem';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Axios from 'axios'
import { Link } from 'react-router-dom'
//import 'react-inputs-validation/lib/react-inputs-validation.min.css';


const initialValues={
  bookingId: 0,
  passportNo:'',
  firstName: '',
  lastName: '',
  mealPref:'',
  age:0,
  gender:'',
}
const dis2=false

const AddressForm=(props)=>{

  const [values, setValues] = useState(initialValues)
  const bookingId = props.id;
  const [count , setCount] = useState(0);
  const [disable, setDisable] = useState(false);
  
  console.log(bookingId);
  const handleInputChange =(event)=>{
      setValues({
          ...values, [event.target.name]:  event.target.value
      })

  }


const handleClick =async(event)=>{
  event.preventDefault()
  console.log(values)
  const temp ={ bookingId:parseInt(values.bookingId),
      firstName:values.firstName,
      lastName:values.lastName,
      mealPref:values.mealPref,
      age:parseInt(values.age),
      passportNo:parseInt(values.passportNo),
      gender:values.gender
      }
  console.log(temp)
  Axios.get('http://localhost:8765/flight-search-booking-service/booking/getbookingbyid/'+bookingId)
  .then((resp)=>{
    console.log(resp.data);
    const booking = resp.data;
    Axios.post('http://localhost:8765/flight-search-booking-service/Passenger/adddetails', temp )
      .then((response)=>{
          console.log(response);
          setCount(count+1);
          if(count>=booking.noOftickets)
          {
            setDisable(true);
            //btn=false;
            
          }
      })

  })
  
}

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Traveller Details
        
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            name="firstName"
            label="First name"
            value={values.firstName}
            helperText="Name should be same as on Govt. ID"
            fullWidth
            onChange={handleInputChange}
            
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="lastName"
            name="lastName"
            label="Last name"
            value={values.lastName}
            onChange ={handleInputChange}
            fullWidth

          />
          </Grid>
          <Grid item xs={12} sm={6}>
          <TextField
            required
            id="passportNo"
            name="passportNo"
            label="Passport No"
            value={values.passportNo}
            onChange ={handleInputChange}
            fullWidth

          />
          </Grid>

        <Grid item xs={12} sm={6}>
        <TextField label="gender" value={values.gender} name="gender" onChange={handleInputChange} fullWidth select>
                         <MenuItem value="Male" name="gender" onChange = {handleInputChange} >Male</MenuItem>
                         <MenuItem value="Female" name="gender" onChange = {handleInputChange}>Female</MenuItem>
                         <MenuItem value="Other" name="gender" onChange = {handleInputChange}>Other</MenuItem>
                          </TextField>
                
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="age"
            name="age"
            label="Age"
            value={values.age}
            onChange ={handleInputChange}
            fullWidth

          />
          </Grid>

        <Grid item xs={12}>
        <TextField label="Meal Preference" value={values.mealPref} name="mealPref" onChange={handleInputChange} fullWidth select>
                         <MenuItem value="Veg" name="mealPref" onChange = {handleInputChange} >Veg</MenuItem>
                         <MenuItem value="Non-Veg" name="mealPref" onChange = {handleInputChange}>Non-Veg</MenuItem>
                         <MenuItem value="None" name="mealPref" onChange = {handleInputChange}>None</MenuItem>
                         
                          </TextField>
                
        </Grid>
        <Button 

variant = "contained"
type = 'submit'
color = "primary"
disabled ={disable}
onClick ={handleClick}>Add <Link to ="/payment"></Link>

</Button>

      </Grid>
    </React.Fragment>
  )
}
  export default AddressForm;
export {dis2}