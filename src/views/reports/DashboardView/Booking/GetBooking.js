import React, { useState, useEffect } from 'react'
import Axios from 'axios';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles({
    root: {
      width: '100%',
    //   alignItems : 'center',
    //   justifyContent : 'center'
    },
    container: {
      maxHeight: 440,
    },
  });

const GetBooking=()=>{

    const classes = useStyles();
    const [booking, setBooking]= useState([])
    const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(4);
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
    

    useEffect(()=>{
        getBooking();
    },[])

    const getBooking= ()=>{
        Axios.get('http://localhost:8765/flight-search-booking-service/booking/getall')
            .then((response)=>{
                console.log(response)
                setBooking(response.data)
            })
    }

    return(
        <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
                <TableCell>Person Id</TableCell>
                <TableCell> Booking Id</TableCell>
                <TableCell> Date of Travel</TableCell>
                <TableCell> Arrival Location</TableCell>
                <TableCell> Departure Location</TableCell>
                <TableCell> no of tickets</TableCell>
                <TableCell> seat type</TableCell>
                <TableCell>Booking Status</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {booking.map((booking) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1}>
                    <TableCell>{booking.personId}</TableCell>
                    <TableCell>{booking.bookingId}</TableCell>
                    <TableCell>{booking.dateOfTravel}</TableCell>
                    <TableCell>{booking.arrivalLocation}</TableCell>
                    <TableCell>{booking.departureLocation}</TableCell>
                    <TableCell>{booking.noOftickets}</TableCell>
                    <TableCell>{booking.seatType}</TableCell>
                    <TableCell>{booking.bookingStatus}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={booking.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
    )
}


export default GetBooking