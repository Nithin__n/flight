import React, { useState } from 'react'
import { FormControl, FormLabel, FormGroup, FormControlLabel,MenuItem, Paper, makeStyles, TextField, Grid, Button } from '@material-ui/core'
import Axios from 'axios'
import { Link , Redirect, useParams,useNavigate } from 'react-router-dom'

const initialValues={
    personId: 0,
    flightNo: 0,
    arrivalLocation: '',
    departureLocation: '',
    noOfticketsg:0,
    noOfticketsr:0,
        dateOfTravelg:'',
        dateOfTravelr:'',
        seatTypeg:'',
        seatTyper:''

}
const useStyles= makeStyles((theme)=>({
    root :{
        display : "flex",
        flexGrow: 1,
        flexWrap: 'wrap',
        '& .MuiInputBase-root ' :{
            width : '75ch',
            margin : theme.spacing(1.5),
              }
           },
    textField :{
        width : '50ch'
    },
    paper:{
        elevation :3,
        margin: theme.spacing(15),
        marginTop :theme.spacing(5),
      width: theme.spacing(90),
      height: theme.spacing(80),
    }
 

}))

const AddBooking=()=>{
    const navigate= useNavigate();
    const {fid,arr,dep,date,st1,way,rdate,st2,rid} = useParams();
    
   
    const classes= useStyles();
    const [values, setValues] = useState(initialValues)
    const[redirect, setRedirect] = useState(false);
    const[id , setId] = useState(0);
    const handleInputChange =(event)=>{
        setValues({
            ...values, [event.target.name]:  event.target.value
        })
 }
         const handleClick =async(event)=>{
        event.preventDefault()
        console.log(values)
        const temp ={ personId:parseInt(values.personId),
            flightNo:parseInt(fid),
            arrivalLocation:arr,
            departureLocation:dep,
            noOftickets:parseInt(values.noOfticketsg),
                dateOfTravel:date,
                seatType:st1
            }
            const temp2 ={ personId:parseInt(values.personId),
                flightNo:parseInt(rid),
                arrivalLocation:dep,
                departureLocation:arr,
                noOftickets:parseInt(values.noOfticketsr),
                    dateOfTravel:rdate,
                    seatType:st2
                }
        console.log(temp)
        console.log(' i am in ouside')
      if(way == "oneWay") {
        Axios.post('http://localhost:8765/flight-search-booking-service/booking/addBooking', temp )
            .then((response)=>{
                console.log(response);
                console.log(' i am in booking')
                setId(response.data.bookingId)
                navigate('/checkout/'+ id, { replace: true })
            }).catch((err)=>{
                console.log(err)
            })
        }
        else {
            Axios.post('http://localhost:8765/flight-search-booking-service/booking/addBooking', temp )
            .then((response)=>{
                setId(response.data.bookingId)
                console.log(response);
                Axios.post('http://localhost:8765/flight-search-booking-service/booking/addBooking', temp2 )
                .then((resp)=>{
                    setRedirect(true);})
            })  
        }
       
    }
    return(

                <div className= {classes.root} >
        
           
                <Grid direction={'column'} spacing={24}
                justify ="center"
                 >
                    <Grid 
                    item xs ={12}   >
                    <Paper className= {classes.paper}>
                    <form onSubmit={handleClick}>
                        <div style ={ {marginLeft:'30px', marginTop :'10px'}}>
                        <TextField  
                       
                        variant= "outlined"
                        label =" person Id"
                        value= {values.personId}
                        name= "personId"
                        onChange ={handleInputChange}
                         style={{ width: 500 }}
                         fullWidth

                        />
                        
                        <TextField  
                        variant= "outlined"
                        label =" no of tickets"
                        value= {values.noOfticketsg}
                        name= "noOfticketsg"
                        onChange = {handleInputChange}
                        />
                        {way=="twoWay" && <TextField  
                        variant= "outlined"
                        label =" no of tickets"
                        value= {values.noOfticketsr}
                        name= "noOfticketsr"
                        onChange = {handleInputChange}
                        />
}
                        
                        <div>
                        <Button 

                        variant = "contained"
                        type = 'submit'
                        color = "primary"
                        onClick ={handleClick}>Add<Link to ="/payment"></Link></Button>
                        </div>
                        </div>
                        </form>
                       </Paper> 
                    </Grid>
                </Grid>
            </div>
    )
}
export default AddBooking