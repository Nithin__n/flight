import React ,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import AddressForm from './AddressForm';
import {PaymentForm, confirmStatus} from './PaymentForm';
import { useParams } from 'react-router-dom';
import disable from './PaymentForm';
import Axios from 'axios';
import {connect} from 'react-redux'
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const mapStateToProps= (state)=>{
  return {user: state.user}

}



const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  stepper: {
    padding: theme.spacing(3, 0, 5),
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
}));

const steps = ['Traveller Details', 'Payment details'];


 const Checkout= (props)=>{
   
  const {bookingId} = useParams('id');
  function getStepContent(step) {
    switch (step) {
       case 0:
        return <AddressForm id={bookingId} />;
      case 1:
        return <PaymentForm  confirmStatus={confirmStatus} ticketsNo={ticketsNo} flightId={flightId} seatType={seatType} />;
      default:
        throw new Error('Unknown step');
    }
  }
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
const[isValid, setIsValid]=useState(true)
  let {
    ticketsNo,
    flightId,
    date,
    arrival,
    departure,
    seatType,
    way,
    rDate,
    rseatType,
    rflightId
  } = useParams();

  // const getStatus=()=>{
  //   console.log(confirmStatus)
  // }
  const confirmStatus=( status)=>{
    console.log(status)
setIsValid(status)
  // return status;
  }
  

  const handleNext = () => {
    setActiveStep(activeStep + 1);
  };
  const handleSubmit=()=>{

    
    Axios.post('http://localhost:8765/flight-search-booking-service/booking/addBooking', {
      "personId": props.user[0].id,
      "flightNo":flightId,
      "dateOfTravel": date,
      "departureLocation": departure,
      "arrivalLocation":arrival,
      "noOftickets": ticketsNo,
      "seatType": seatType
  })
    .then((response)=>{
      console.log(response.data)
      setActiveStep(activeStep + 1);
      console.log( ticketsNo,
        flightId,
        date,
        arrival,
        departure,
        seatType,
        way,
        rDate,
        rseatType,
        rflightId
        )
        console.log('booking sucessful')
    }).catch((err)=>{
      console.log(err)
    })
   
   
    
  }

  const handleBack = () => {
    
    setActiveStep(activeStep - 1);
    
  };

  return (
    <React.Fragment>
      <CssBaseline />

      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Typography component="h1" variant="h4" align="center">
            Book Ticket
          </Typography>
          <Stepper activeStep={activeStep} className={classes.stepper}>
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          <React.Fragment>
            {activeStep === steps.length ? (
              <React.Fragment>
                <Typography variant="h5" gutterBottom>
                   Booking Successful.
                </Typography>
               
              </React.Fragment>
            ) : (
              <React.Fragment>
                {getStepContent(activeStep)}
                <div className={classes.buttons}>
                  {activeStep !== 0 && (
                    <Button onClick={handleBack} className={classes.button}>
                      Back
                    </Button>
                  )}
                  {
                    activeStep ==0  && <Button
                    variant="contained"
                    color="primary"
                    
                    onClick={handleNext}
                    className={classes.button}
                    
                  >
                    Next
                  </Button>
                  }
                  {activeStep ==1 &&
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={isValid}
                    onClick={handleSubmit}
                    className={classes.button}
                    
                  >
                    Confirm Booking
                  </Button>
                  }
                </div>
              </React.Fragment>
            )}
          </React.Fragment>
        </Paper>
        <Copyright />
      </main>
    </React.Fragment>
  );
}
export default connect(mapStateToProps)(Checkout)




