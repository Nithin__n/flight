import Axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Link, useParams, useNavigate, Navigate } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import {
  FormLabel,
  FormGroup,
  FormControlLabel,
  TextField,
  Grid
} from '@material-ui/core';
import Search2 from './search_flight';

const useStyles = makeStyles({
  root: {
    width: '100%'
    //   alignItems : 'center',
    //   justifyContent : 'center'
  },
  container: {
    maxHeight: 440
  }
});

const Flights = props => {
  const [open, setOpen] = useState(false);
  const [ticketsNo, setTicketsNo] = useState(0);
  const navigate = useNavigate();
  const classes = useStyles();
  const [rowsPerPage, setRowsPerPage] = useState(4);
  const [page, setPage] = useState(0);
  const [flightId, setFlightId]= useState(0);
  const [values, setValues] = useState({
    list: [],
    rlist: [],
    seatType: '',
    rseatType: ''
  });

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  let {
    date,
    arrival,
    departure,
    seatType,
    way,
    rDate,
    rseatType
  } = useParams();

  const paramValues = {
    date: date,
    arrival: arrival,
    departure: departure,
    seatType: seatType,
    way: way,
    rDate: rDate,
    rseatType: rseatType
  };
  const style1 = {
    color: 'azure',
    'background-color': '#6600cc'
  };

  const tab_con = {
    'margin-top': '100px'
  };

  const getFlightDetails = () => {
    if (paramValues.way == 'oneWay') {
      Axios.get(
        'http://localhost:8010/search/date/' +
          paramValues.date +
          '/dep/' +
          paramValues.departure +
          '/arr/' +
          paramValues.arrival
      ).then(response => {
        console.log(response.data);
        setValues({ ...values, list: response.data });
      });
    }
  };

  useEffect(() => {
    console.log(props);
    getFlightDetails();
  }, []);

  const handleAddBooking = idx => {
    const id = values.list[idx].flightId;
    setFlightId(id);
    setOpen(true);
  };

  const handleTicketChange = event => {
    setTicketsNo(event.target.value);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleSubmit = () => {
    console.log(flightId, ticketsNo)
    navigate(
      '/app/dashboard/AddBooking/' +
      ticketsNo +
      '/' +
        flightId +
        '/' +
        arrival +
        '/' +
        departure +
        '/' +
        date +
        '/' +
        seatType +
        '/' +
        way +
        '/' +
        null +
        '/' +
        null +
        '/' +
        null,
      { replace: true }
    );
  };
  return (
    <div>
      <div>
        <Paper className={classes.root}>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <TableCell>Flight Id</TableCell>
                  <TableCell>Departure Location </TableCell>
                  <TableCell>Arrival Location</TableCell>
                  <TableCell>Remaining Seat</TableCell>
                  <TableCell> Fare</TableCell>
                  <TableCell> Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {
                  values.list.map((flight, idx) => {
                    return (
                      <TableRow hover role="checkbox" key={flight.flightId}>
                        <TableCell>{flight.flightId}</TableCell>
                        <TableCell>{flight.arrivalLocation.name}</TableCell>
                        <TableCell>{flight.departureLocation.name}</TableCell>
                        {seatType && seatType == 'Economy' && (
                          <TableCell>
                            {flight.flightStatus.remainingEconomySeats}
                          </TableCell>
                        )}
                        {seatType && seatType == 'Business' && (
                          <TableCell>
                            {flight.flightStatus.remainingBusinessSeats}
                          </TableCell>
                        )}
                        {seatType && seatType == 'Premium' && (
                          <TableCell>
                            {flight.flightStatus.remainingPremiunSeats}
                          </TableCell>
                        )}
                        {seatType && seatType == 'Economy' && (
                          <TableCell>{flight.fare.economyFare}</TableCell>
                        )}
                        {seatType && seatType == 'Business' && (
                          <TableCell>{flight.fare.premiumfare}</TableCell>
                        )}
                        {seatType && seatType == 'Premium' && (
                          <TableCell>{flight.fare.businessFare}</TableCell>
                        )}
                        <TableCell>
                          <Button
                            // variant
                            onClick={() => {
                              handleAddBooking(idx);
                            }}
                          >
                            ADD Booking
                          </Button>
                        </TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>

          <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={values.list.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </Paper>
      </div>
      <div>
        <Dialog open={open} onClose={handleClose}>
          <DialogTitle id="alert-dialog-title">{'Booking Details'}</DialogTitle>
          <DialogContent>
            <div className={classes.root}>
              <Grid container spacing={2} justify="center">
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <form>
                      <div
                        style={{
                          marginLeft: '30px',
                          marginTop: '10px'
                        }}
                      >
                        <TextField
                          variant="outlined"
                          label=" No of Tickets "
                          value={ticketsNo}
                          name="ticketsNo"
                          onChange={handleTicketChange}
                        />
                      </div>
                    </form>
                  </Paper>
                </Grid>
              </Grid>
            </div>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Disagree
            </Button>
            <Button onClick={handleSubmit} color="primary" autoFocus>
              Add Booking
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </div>
  );
};

export default Flights;

// import Axios from 'axios'
// import  React, { useState, useEffect }  from 'react'
// import { Link, useParams } from 'react-router-dom'
// import Search2 from './search_flight'

// const Flights=(props)=>{
//     const [values, setValues]= useState({
//             list:[],
//             rlist:[],
//             seatType:'',
//             rseatType:''
//     })
//     // const date= useParams('date')
//   let  {date,arrival,departure,seatType,oneWay, rDate,rseatType}= useParams();
//   console.log(date,arrival,departure,seatType,oneWay, rDate,rseatType)
//     //  const    {arrival1} =useParams('arrival');
//     //    const  departure1= useParams('departure');
//     //    const  seatType1= useParams('seatType');
//     //    const  oneWay1= useParams('oneWay');
//     //    const  rDate1 = useParams('rDate');
//     //    const  rseatType1= useParams('rseatType');

//     const paramValues={
//         date: date,
//          arrival: arrival,
//          departure: departure,
//          seatType: seatType,
//          oneWay: oneWay,
//          rDate :rDate,
//          rseatType: rseatType
//     }
//     const style1={
//         "color": "azure",
//         "background-color":"#6600cc"
//     }

//     const tab_con={
//         "margin-top":"100px"
//       }

//       const getFlightDetails=()=>{
//         if(paramValues.oneWay=="true"){
//             Axios.get('http://localhost:8010/search/date/'+paramValues.date+'/dep/'+paramValues.departure+'/arr/'+paramValues.arrival)
//                 .then(response=>{
//                     setValues({...values, list:response.data})
//                 })

//                 setValues({...values,
//                     seatType:paramValues.seatType
//                 })
//             }

//             if(paramValues.oneWay=="false"){
//                 Axios.post('http://localhost:8010/search', {
//                     oneWay:false,
//                     depCity:paramValues.departure,
//                     arrCity:paramValues.arrival,
//                     goingDate:paramValues.date,
//                     returnDate:paramValues.rDate
//                 })
//                 .then( response => {
//                    setValues({
//                     list:response.data.going,
//                     rlist:response.data.return
//                    })
//                 })

//                 setValues({
//                     seatType:paramValues.seatType,
//                     rseatType:paramValues.rseatType
//                 })
//                 }
//       }

//       useEffect(()=>{
//         console.log(props)
//         getFlightDetails()
//     },[])

//    const changeState=(oneWay,arrival,departure,date,rDate,seatType,rseatType)=>{
//         if(oneWay=="true"){
//             Axios.get('http://localhost:8010/search/date/'+date+'/dep/'+departure+'/arr/'+arrival)
//             .then(response=>{
//                 setValues({...values,
//                 list:response.data,
//                 seatType:seatType
//              })
//             })

//         }
//         else{
//             Axios.post('http://localhost:8010/search', {
//                 oneWay:false,
//                 depCity:departure,
//                 arrCity:arrival,
//                 goingDate:date,
//                 returnDate:rDate
//             })
//             .then( response => {
//                 console.log(response.data);
//                 setValues({...values,
//                     list:response.data.going,
//                     rlist:response.data.return,
//                     seatType:seatType,
//                     rseatType:rseatType
//                  })
//             })

//         }
//     }

//     const departList=values.list.map((flight)=>(
//         <tr>
//             <td>{flight.fleet.code}</td>
//             <td>
//                 <h5>{flight.departureLocation.name}  ({flight.departureLocation.country})</h5>
//                 {flight.departureTime} <br />
//                 {flight.departureLocation.airportName}
//             </td>
//              <td>
//                 <h5>{flight.arrivalLocation.name} ({flight.arrivalLocation.country})</h5>
//                 {flight.arrivalTime} <br />
//                 {flight.arrivalLocation.airportName}
//             </td>

//              <td>
//                 {values.seatType=='Economy' &&  <b>{flight.flightStatus.remainingEconomySeats}</b>  }
//                 {values.seatType=='Business' && <b>{flight.flightStatus.remainingBusinessSeats}</b> }
//                 {values.seatType=='Premium'&& <b>{flight.flightStatus.remainingPremiunSeats}</b>}
//             </td>

//              <td>
//                  {values.seatType=='Economy' &&    <b>{flight.fare.economyFare}</b>}
//                 {values.seatType=='Business' && <b>{flight.fare.businessFare}</b>}
//                 {values.seatType=='Premium'&& <b>{flight.fare.premiumfare}</b>}
//              </td>

//              <td>
//                 <Link class="btn mr-2" style={style1} to={'/addBooking/'+flight.flightId+'/'+flight.arrivalLocation.name+'/'+flight.departureLocation.name+'/'+flight.date+'/'+flight.seatType+'/true/'+'null/'+'null/null'}>Book</Link>
//              </td>
//         </tr>
//          ))

//          const finalList=values.list.map((flight)=>(
//             values.rlist.map((rflight)=>(
//                 <>
//                 <tr>
//                     <td>{flight.fleet.code}</td>
//                     <td>
//                         <h5>{flight.departureLocation.name}  ({flight.departureLocation.country})</h5>
//                         {flight.departureTime} <br />
//                         {flight.departureLocation.airportName}
//                     </td>
//                     <td>
//                         <h5>{flight.arrivalLocation.name} ({flight.arrivalLocation.country})</h5>
//                         {flight.arrivalTime} <br />
//                         {flight.arrivalLocation.airportName}
//                     </td>

//                     <td>
//                         {values.seatType=='Economy' &&  <b>{flight.flightStatus.remainingEconomySeats}</b>  }
//                          {values.seatType=='Business' && <b>{flight.flightStatus.remainingBusinessSeats}</b> }
//                         {values.seatType=='Premium'&& <b>{flight.flightStatus.remainingPremiunSeats}</b>}
//                     </td>

//                     <td>
//                         {values.seatType=='Economy' &&    <b>{flight.fare.economyFare}</b>}
//                         {values.seatType=='Business' && <b>{flight.fare.businessFare}</b>}
//                         {values.seatType=='Premium'&& <b>{flight.fare.premiumfare}</b>}
//                     </td>

//                      <td>
//                         <Link class="btn mr-2" style={style1} to={'/addBooking/'+flight.flightId+'/'+flight.arrivalLocation.name+'/'+flight.departureLocation.name+'/'+flight.date+'/'+this.state.seatType+'/false/'+rflight.date+'/'+this.state.rseatType+'/'+rflight.flightId}>Book</Link>
//                     </td>
//                 </tr>
//                 <tr>
//                     <td>{rflight.fleet.code}</td>
//                      <td>
//                         <h5>{rflight.departureLocation.name}  ({rflight.departureLocation.country})</h5>
//                         {rflight.departureTime} <br />
//                         {rflight.departureLocation.airportName}
//                     </td>
//                     <td>
//                         <h5>{rflight.arrivalLocation.name} ({rflight.arrivalLocation.country})</h5>
//                         {rflight.arrivalTime} <br />
//                         {rflight.arrivalLocation.airportName}
//                     </td>

//                     <td>
//                         {values.rseatType=='Economy' &&  <b>{rflight.flightStatus.remainingEconomySeats}</b>  }
//                         {values.rseatType=='Business' && <b>{rflight.flightStatus.remainingBusinessSeats}</b> }
//                         {values.rseatType=='Premium'&& <b>{rflight.flightStatus.remainingPremiunSeats}</b>}
//                     </td>

//                     <td>
//                         {values.rseatType=='Economy' &&  <b>{rflight.fare.economyFare}</b>}
//                         {values.rseatType=='Business' && <b>{rflight.fare.businessFare}</b>}
//                         {values.rseatType=='Premium'&& <b>{rflight.fare.premiumfare}</b>}
//                     </td>
//                     <td>

//                      </td>
//                 </tr>
//                 </>

//             ))

//      ))
//     return (
//         <div>
//         <Search2 values={paramValues} changeState={changeState} />
//   <div className="container" style={tab_con}>
//         {paramValues.oneWay=="true" &&
//         <table className="table table-bordered vertical">
//           <thead style={style1}>
//               <tr>
//                   <th scope="col"> </th>
//                   <th scope="col">Departure</th>
//                   <th scope="col">Arrival</th>
//                   <th scope="col">Remaining Seats</th>
//                   <th scope="col">Fares</th>
//                   <th scope="col"></th>
//               </tr>
//           </thead>
//           <tbody>
//               {departList}
//               {paramValues.oneWay=="false" && finalList}

//            </tbody>

//          </table>
//       }
//       {paramValues.oneWay=="false" &&
//       <table className="table table-striped table-bordered">
//           <thead style={style1}>
//               <tr>
//                   <th scope="col"> </th>
//                   <th scope="col">Departure</th>
//                   <th scope="col">Arrival</th>
//                   <th scope="col">Remaining Seats</th>
//                   <th scope="col">Fares</th>
//                   <th scope="col"></th>
//               </tr>
//           </thead>
//           <tbody>
//               {finalList}
//           </tbody>

//       </table>
//       }
//   </div>
//   </div>
//     )
// }
