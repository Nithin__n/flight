import React, { useState, useEffect } from 'react'
import Axios from 'axios';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { red } from '@material-ui/core/colors';
import {connect} from 'react-redux'
import { Divider } from '@material-ui/core';

const useStyles = makeStyles({
    root: {
      width: '100%',
    //   alignItems : 'center',
    //   justifyContent : 'center'
    },
    container: {
      maxHeight: 440,
    },


  });

const GetBooking=(props)=>{

    const classes = useStyles();
    const[render , setRender] = useState(false);
    const [booking, setBooking]= useState([])
    const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(4);
  const [open, setOpen] = useState(false);
  const userId= props.user[0].id;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  
  


  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
    useEffect(()=>{
        getBooking();
    },[])

    async function getBooking(){
      
        await Axios.get('http://localhost:8765/user-service/user/'+userId+ '/dashboard')
            .then((response)=>{
                console.log(response.data)
                setBooking(response.data.booking)
            })
    }


    async function handleCancel(bookingId) {
    await Axios.delete('http://localhost:8765/user-service/user/' + userId +'/booking/' +bookingId).then(
      (response) => {
        console.log(response.data);
        getBooking();
        handleClose();
      }
    ).catch(
      (error)=>{
        console.log(error.response.data);
      }
    )
  }

    return(
        <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <h4>Booked Details</h4>
            
            <TableRow>
                <TableCell> Booking Id</TableCell>
                <TableCell> Date of Travel</TableCell>
                <TableCell> Arrival Location</TableCell>
                <TableCell> Departure Location</TableCell>
                <TableCell> no of tickets</TableCell>
                <TableCell> seat type</TableCell>
                <TableCell>Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {booking.map((booking) => {
              return (
                booking.bookingStatus=="Cancelled"?
                <TableRow selected hover role="checkbox" tabIndex={-1} key={booking.bookingId}>
                    <TableCell>{booking.bookingId}</TableCell>
                    <TableCell>{booking.dateOfTravel}</TableCell>
                    <TableCell>{booking.arrivalLocation}</TableCell>
                    <TableCell>{booking.departureLocation}</TableCell>
                    <TableCell>{booking.noOftickets}</TableCell>
                    <TableCell>{booking.seatType}</TableCell>
                    <TableCell>Cancelled</TableCell>
                </TableRow>
                :
                <TableRow hover role="checkbox" tabIndex={-1} key={booking.bookingId}>
                    <TableCell>{booking.bookingId}</TableCell>
                    <TableCell>{booking.dateOfTravel}</TableCell>
                    <TableCell>{booking.arrivalLocation}</TableCell>
                    <TableCell>{booking.departureLocation}</TableCell>
                    <TableCell>{booking.noOftickets}</TableCell>
                    <TableCell>{booking.seatType}</TableCell>
                    <TableCell><Button onClick={handleClickOpen} variant="outlined" color="secondary">Cancel</Button>
                    <Dialog
                      open={open}
                      onClose={handleClose}
                      aria-labelledby="alert-dialog-title"
                      aria-describedby="alert-dialog-description"
                    >
                      <DialogTitle id="alert-dialog-title">{"Are you sure you want to cancel this booking?"}</DialogTitle>
                      <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                          Booking cancellation is permanent and cannot be reversed.
                        </DialogContentText>
                      </DialogContent>
                      <DialogActions>
                        <Button onClick={handleClose} color="primary">
                          Return
                        </Button>
                        <Button onClick={() => handleCancel(booking.bookingId)} color="primary" autoFocus>
                          Cancel Ticket
                        </Button>
                      </DialogActions>
                    </Dialog>
                    </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={booking.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
    )
}


const mapStateToProps= (state)=>{
      return {user: state.user}
    
    }
  
  export default connect(mapStateToProps)(GetBooking)





// import React, { useState, useEffect } from 'react'
// import Axios from 'axios';
// import Paper from '@material-ui/core/Paper';
// import Table from '@material-ui/core/Table';
// import TableBody from '@material-ui/core/TableBody';
// import TableCell from '@material-ui/core/TableCell';
// import TableContainer from '@material-ui/core/TableContainer';
// import TableHead from '@material-ui/core/TableHead';
// import TablePagination from '@material-ui/core/TablePagination';
// import TableRow from '@material-ui/core/TableRow';
// import { makeStyles } from '@material-ui/core/styles';
// import Button from '@material-ui/core/Button';
// import Dialog from '@material-ui/core/Dialog';
// import DialogActions from '@material-ui/core/DialogActions';
// import DialogContent from '@material-ui/core/DialogContent';
// import DialogContentText from '@material-ui/core/DialogContentText';
// import DialogTitle from '@material-ui/core/DialogTitle';
// import {connect} from 'react-redux'
// const useStyles = makeStyles({
//     root: {
//       width: '100%',
//     //   alignItems : 'center',
//     //   justifyContent : 'center'
//     },
//     container: {
//       maxHeight: 440,
//     },
//   });

// const BookedTable=(props)=>{
// console.log(props.user)
// const userId= props.user[0].id;
//     const classes = useStyles();
//     const[render , setRender] = useState(false);
//     const [booking, setBooking]= useState([])
//     const [page, setPage] = useState(0);
//   const [rowsPerPage, setRowsPerPage] = useState(4);
//   const [open, setOpen] = React.useState(false);

//   const handleChangePage = (event, newPage) => {
//     setPage(newPage);
//   };

//   const handleChangeRowsPerPage = (event) => {
//     setRowsPerPage(+event.target.value);
//     setPage(0);
//   };
  
//   const handleClickOpen = () => {
//     setOpen(true);
//   };

  
//   const handleClose = () => {
//     setOpen(false);
//   };
//     useEffect(()=>{
//         getBooking();
//     },[])

//     async function getBooking(){
//         await Axios.get('http://localhost:8765/user-service/user/'+ userId+ '/dashboard')
//             .then((response)=>{
//                 console.log(response.data)
//                 setBooking(response.data.booking)
//             })
//     }


//     async function handleCancel(bookingId) {
//     await Axios.delete('http://localhost:8765/user-service/user/'+userId +'/booking/' +bookingId).then(
//       (response) => {
//         console.log(response.data);
//         getBooking();
//         handleClose();
//       }
//     ).catch(
//       (error)=>{
//         console.log(error.response.data);
//       }
//     )
//   }

//     return(
//         <Paper className={classes.root}>
//       <TableContainer className={classes.container}>
//         <Table stickyHeader aria-label="sticky table">
//           <TableHead>
//             <TableRow> <h4>Booked Ticket Details </h4></TableRow> <hr></hr>
//             <TableRow>
//                 <TableCell> Booking Id</TableCell>
//                 <TableCell> Date of Travel</TableCell>
//                 <TableCell> Arrival Location</TableCell>
//                 <TableCell> Departure Location</TableCell>
//                 <TableCell> no of tickets</TableCell>
//                 <TableCell> seat type</TableCell>
//                 <TableCell>Action</TableCell>
//             </TableRow>
//           </TableHead>
//           <TableBody>

//             {booking.map((booking) => {
//               return (
//                 booking.bookingStatus=="Cancelled"?null:
//                 <TableRow hover role="checkbox" tabIndex={-1}>
//                     <TableCell>{booking.bookingId}</TableCell>
//                     <TableCell>{booking.dateOfTravel}</TableCell>
//                     <TableCell>{booking.arrivalLocation}</TableCell>
//                     <TableCell>{booking.departureLocation}</TableCell>
//                     <TableCell>{booking.noOftickets}</TableCell>
//                     <TableCell>{booking.seatType}</TableCell>
//                     <TableCell><Button onClick={handleClickOpen} variant="outlined" color="secondary">Cancel</Button>
//                     <Dialog
//                       open={open}
//                       onClose={handleClose}
//                       aria-labelledby="alert-dialog-title"
//                       aria-describedby="alert-dialog-description"
//                     >
//                       <DialogTitle id="alert-dialog-title">{"Are you sure you want to cancel this booking?"}</DialogTitle>
//                       <DialogContent>
//                         <DialogContentText id="alert-dialog-description">
//                           Booking cancellation is permanent and cannot be reversed.
//                         </DialogContentText>
//                       </DialogContent>
//                       <DialogActions>
//                         <Button onClick={handleClose} color="primary">
//                           Return
//                         </Button>
//                         <Button onClick={() => handleCancel(booking.bookingId)} color="primary" autoFocus>
//                           Cancel Ticket
//                         </Button>
//                       </DialogActions>
//                     </Dialog>
//                     </TableCell>
//           </TableRow>
//               );
//             })}
//           </TableBody>
//         </Table>
//       </TableContainer>
//       <TablePagination
//         rowsPerPageOptions={[10, 25, 100]}
//         component="div"
//         count={booking.length}
//         rowsPerPage={rowsPerPage}
//         page={page}
//         onChangePage={handleChangePage}
//         onChangeRowsPerPage={handleChangeRowsPerPage}
//       />
//     </Paper>
//     )
// }
// const mapStateToProps= (state)=>{
//     return {user: state.user}
  
//   }

// export default connect(mapStateToProps)(BookedTable)