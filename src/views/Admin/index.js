import React from 'react';
import PropTypes from 'prop-types';
import { Card, Box,Container, Grid , makeStyles} from '@material-ui/core';
import { Link } from 'react-router-dom';
import Page from 'src/components/Page';
import LocationCard from './LocationCard';
import FleetCard from './FleetCard';
import FareCard from './FareCard';
import FlightCard from './FlightCard';
import StatusCard from './StatusCard'

const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: theme.palette.background.dark,
      minHeight: '100%',
      paddingBottom: theme.spacing(3),
      paddingTop: theme.spacing(3)
    }
  }));
const AdminDashBoard = props => {

    const classes= useStyles();
    return (
    
    <div>
        <div class="container">
          <div class ="row">
            
        
          <div class="card text-white bg-primary mb-3 col-3 " style={{"margin-left":'20px' ,"margin-top":'20px',  "max-width" : "18rem"}}>
            <div class="card-body">
              {/* <a  style={{"color":"white", "text-decoration" : "none"} } href="/app/admin/location"><h1>Location</h1></a> */}
              <Link to ="/app/admin/location" style={{"color":"white", "textDecoration" : "none"}}><h2>Location</h2></Link>
            </div>
          </div>

          <div class="card text-white bg-primary mb-3 col-3" style={{"margin-left":'20px' ,"margin-top":'20px',  "max-width" : "18rem"}}>
            <div class="card-body">
              {/* <a  style={{"color":"white", "text-decoration" : "none"} } href="/app/admin/location"><h1>Location</h1></a> */}
              <Link to ="/app/admin/fleet" style={{"color":"white", "textDecoration" : "none"}}><h2>Fleet</h2></Link>
            </div>
          </div>

          <div class="card text-white bg-primary mb-3 col-3" style={{"margin-left":'20px' ,"margin-top":'20px',  "max-width" : "18rem"}}>
            <div class="card-body">
              {/* <a  style={{"color":"white", "text-decoration" : "none"} } href="/app/admin/location"><h1>Location</h1></a> */}
              <Link to ="/app/admin/fare" style={{"color":"white", "textDecoration" : "none"}}><h3>Fare</h3></Link>
            </div>
          </div>
          </div>
          <div class ="row">
          <div class="card text-white bg-primary mb-3 col-3" style={{"margin-left":'20px' ,"margin-top":'20px',  "max-width" : "18rem"}}>
            <div class="card-body">
              {/* <a  style={{"color":"white", "text-decoration" : "none"} } href="/app/admin/location"><h1>Location</h1></a> */}
              <Link to ="/app/admin/status" style={{"color":"white", "textDecoration" : "none"}}><h3>Status</h3></Link>
            </div>
          </div>
          
          <div class="card text-white bg-primary mb-3 col-3" style={{"margin-left":'20px' ,"margin-top":'20px',  "max-width" : "18rem"}}>
            <div class="card-body">
              {/* <a  style={{"color":"white", "text-decoration" : "none"} } href="/app/admin/location"><h1>Location</h1></a> */}
              <Link to ="/app/admin/flight" style={{"color":"white", "align":"center" ,"textDecoration" : "none"}}><h3>Flight</h3></Link>
            </div>
          </div>
          </div>
          
          </div>
    </div>
    
    );
};


export default AdminDashBoard;