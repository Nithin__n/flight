import React, { useState } from 'react';
import {
  FormControl,
  FormLabel,
  FormGroup,
  FormControlLabel,
  Paper,
  makeStyles,
  TextField,
  Grid,
  Button
} from '@material-ui/core';
import {
    Box,
    Card,
    CardContent,
    CardHeader,
    Divider
  } from '@material-ui/core';
import Axios from 'axios';
import { Link, Navigate, useNavigate } from 'react-router-dom';

const initialValues = {
  id: 0,
  airportName: '',
  code: '',
  country: '',
  name: '',
  status: 'active'
};
const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexGrow: 1,
    flexWrap: 'wrap',
    '& .MuiInputBase-root ': {
      width: '75ch',
      margin: theme.spacing(0.5)
    }
  },
  textField: {
    width: '50ch'
  },
  paper: {
    elevation: 3,
    margin: theme.spacing(10),
    marginTop: theme.spacing(5),
    width: theme.spacing(90),
    height: theme.spacing(50)
  }
  //
  // paperContent : {

  //     margin : theme.spacing(5),
  //     padding : theme.spacing(3),
  //     marginLeft: theme.spacing(1),
  // marginRight: theme.spacing(1),
  // width: '25ch',
  // }
}));
const FlightForm = () => {
  const navigate = useNavigate();

  const classes = useStyles();
  const [values, setValues] = useState(initialValues);

  const handleInputChange = event => {
    // const [name, value] = event.target
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };

  const handleSubmit = event => {
    event.preventDefault();
    Axios.post('http://localhost:9001/flight', values).then(response => {
      navigate('/app/admin/flight');
      console.log(response);
    });
  };
  return (
    <Box width="50%">
      <form onSubmit={handleSubmit}>
        <Card style={{ width: '300' }}>
          <CardHeader title="Flight" />
          <Divider />
          <CardContent>
            <Grid container spacing={3}>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  label=" Flight Id"
                  value={values.flightId}
                  name="flightId"
                  onChange={handleInputChange}
                   fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                fullWidth
                  variant="outlined"
                  label="Arrival Time"
                  value={values.arrivalTime}
                  name="arrivalTime"
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                fullWidth
                  variant="outlined"
                  label="Departure Time"
                  value={values.departureTime}
                  name="departureTime"
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                fullWidth
                  variant="outlined"
                  label="Date"
                  value={values.date}
                  name="date"
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                fullWidth
                  variant="outlined"
                  label="Arrival LocationId"
                  value={values.arrivalLocationId}
                  name="arrivalLocationId"
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                fullWidth
                  variant="outlined"
                  label="Departure LocationId"
                  value={values.departureLocationId}
                  name="departureLocationId"
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                fullWidth
                  variant="outlined"
                  label="Fleet Id"
                  value={values.fleetId}
                  name="fleetId"
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                fullWidth
                  variant="outlined"
                  label="Fare Id"
                  value={values.fareId}
                  name="fareId"
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                fullWidth
                  variant="outlined"
                  label="flight StatusId"
                  value={values.flightStatusId}
                  name="flightStatusId"
                  onChange={handleInputChange}
                />
              </Grid>
            </Grid>
          </CardContent>
          <Divider />
          <div></div>
          <Box display="flex" justifyContent="flex-end" p={2}>
            <Button variant="contained" type="submit" color="primary">
              Add
            </Button>
            <Button>
              <Link to="/app/admin/flight">Cancel</Link>
            </Button>
          </Box>
        </Card>
      </form>
    </Box>
  );
};
export default FlightForm;
