import React, { useState } from 'react';
import {
  FormControl,
  FormLabel,
  FormGroup,
  FormControlLabel,
  Paper,
  makeStyles,
  TextField,
  Grid,
  Button
} from '@material-ui/core';
import {
    Box,
    Card,
    CardContent,
    CardHeader,
    Divider
  } from '@material-ui/core';
import Axios from 'axios';
import { Link, Navigate ,useNavigate} from 'react-router-dom';
import Alert from '@material-ui/lab/Alert';

const initialValues = {
  id: 0,
  airportName: '',
  code: '',
  country: '',
  name: '',
  status: 'active'
};

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexGrow: 1,
    flexWrap: 'wrap',
    '& .MuiInputBase-root ': {
      width: '75ch',
      margin: theme.spacing(0.5)
    }
  },
  textField: {
    width: '50ch'
  },
  paper: {
    elevation: 3,
    margin: theme.spacing(10),
    marginTop: theme.spacing(5),
    width: theme.spacing(90),
    height: theme.spacing(50)
  }
  //
  // paperContent : {

  //     margin : theme.spacing(5),
  //     padding : theme.spacing(3),
  //     marginLeft: theme.spacing(1),
  // marginRight: theme.spacing(1),
  // width: '25ch',
  // }
}));
const LocationForm = (props) => {
    const navigate=useNavigate();
  const classes = useStyles();
  const [values, setValues] = useState(initialValues);

  const handleInputChange = event => {
    // const [name, value] = event.target
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };

  const handleSubmit = (event) => {
      event.preventDefault();
    Axios.post('http://localhost:9001/flight/location', values).then(
      response => {
          navigate('/app/admin/location')
        console.log(response);
      }
    );
  };

  return (
    // <div className= {classes.root} >

    //         <Grid container spacing={2}
    //         justify ="center"
    //          >
    //             <Grid
    //             item xs ={12}   >
    //             <Paper className= {classes.paper}>
    //             <form>
    //                 <div style ={ {marginLeft:'30px', marginTop :'10px'}}>
    <Box width="50%">
    <form onSubmit={handleSubmit}>
      <Card style={{width: '300'}}>
        <CardHeader title="Location" />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>

          
         < Grid
              item
              xs={12}
              sm={6}
            >
          <TextField
          fullWidth
            variant="outlined"
            label=" Location Id"
            value={values.id}
            name="id"
            onChange={handleInputChange}
            // style={{ width: 500 }}
            // fullWidth
          />
          </Grid>
          < Grid
              item
              xs={12}
              sm={6}
            >
          <TextField
          fullWidth
            variant="outlined"
            label=" Location Name"
            value={values.name}
            name="name"
            onChange={handleInputChange}
          />
          </Grid>
          < Grid
              item
              xs={12}
              sm={6}
            >
          <TextField
          fullWidth
            variant="outlined"
            label=" Airport Name"
            value={values.airportName}
            name="airportName"
            onChange={handleInputChange}
          />
          </Grid>
          < Grid
              item
              xs={12}
              sm={6}
            >
          <TextField
          fullWidth
            variant="outlined"
            label="code"
            value={values.code}
            name="code"
            onChange={handleInputChange}
          />
          </Grid>
          < Grid
              item
              xs={12}
              sm={6}
            >
          <TextField
          fullWidth
            variant="outlined"
            label=" Country"
            value={values.country}
            name="country"
            onChange={handleInputChange}
          />
          </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <div>
          
        </div>
        <Box display="flex" justifyContent="flex-end" p={2}>
        <Button
            variant="contained"
            type="submit"
            color="primary"
            
          >
            Add
          </Button>
          <Button >
            <Link to="/app/admin/location">Cancel</Link>
          </Button>
        </Box>
      </Card>
    </form>
    </Box>
  );
};
export default LocationForm;
