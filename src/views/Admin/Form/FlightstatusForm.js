import React, { useState } from 'react';
import {
  FormControl,
  FormLabel,
  FormGroup,
  FormControlLabel,
  Paper,
  makeStyles,
  TextField,
  Grid,
  Button
} from '@material-ui/core';
import Axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';
import { Box, Card, CardContent, CardHeader, Divider } from '@material-ui/core';
const initialValues = {
  id: 0,
  airportName: '',
  code: '',
  country: '',
  name: '',
  status: 'active'
};
const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexGrow: 1,
    flexWrap: 'wrap',
    '& .MuiInputBase-root ': {
      width: '75ch',
      margin: theme.spacing(0.5)
    }
  },
  textField: {
    width: '50ch'
  },
  paper: {
    elevation: 3,
    margin: theme.spacing(10),
    marginTop: theme.spacing(5),
    width: theme.spacing(90),
    height: theme.spacing(50)
  }
  //
  // paperContent : {

  //     margin : theme.spacing(5),
  //     padding : theme.spacing(3),
  //     marginLeft: theme.spacing(1),
  // marginRight: theme.spacing(1),
  // width: '25ch',
  // }
}));
const FlightstatusForm = () => {
  const navigate = useNavigate();

  const classes = useStyles();
  const [values, setValues] = useState(initialValues);

  const handleInputChange = event => {
    // const [name, value] = event.target
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    await Axios.post('http://localhost:9001/flight/status', values).
    then(response => {
      navigate('/app/admin/status');
      console.log(response);
    });
  };
  return (
    <Box width="50%">
      <form onSubmit={handleSubmit}>
        <Card style={{ width: '300' }}>
          <CardHeader title="Remaining Seats" />
          <Divider />
          <CardContent>
            <Grid container spacing={3}>
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  variant="outlined"
                  label=" Flightstatus Id"
                  value={values.id}
                  name="id"
                  onChange={handleInputChange}
                  // style={{ width: 500 }}
                  // fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  variant="outlined"
                  label=" Remaining Economy Seats"
                  value={values.remainingEconomySeats}
                  name="remainingEconomySeats"
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  variant="outlined"
                  label="Remaining Premiun Seats"
                  value={values.remainingPremiunSeats}
                  name="remainingPremiunSeats"
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  variant="outlined"
                  label=" Remaining Business Seats"
                  value={values.remainingBusinessSeats}
                  name="remainingBusinessSeats"
                  onChange={handleInputChange}
                />
              </Grid>
            </Grid>
          </CardContent>
          <Divider />
          <div>
          
        </div>
          <Box display="flex" justifyContent="flex-end" p={2}>
            <Button variant="contained" type="submit" color="primary">
              Add
            </Button>
            <Button>
              <Link to="/app/admin/status">Cancel</Link>
            </Button>
          </Box>
        </Card>
      </form>
    </Box>
  );
};
export default FlightstatusForm;
