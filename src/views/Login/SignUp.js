// import React,{useState} from 'react';
// import {BrowserRouter as Router , Link} from 'react-router-dom';
// import Axios from 'axios';
// import Avatar from '@material-ui/core/Avatar';
// import Button from '@material-ui/core/Button';
// import CssBaseline from '@material-ui/core/CssBaseline';
// import TextField from '@material-ui/core/TextField';
// import Grid from '@material-ui/core/Grid';
// import Box from '@material-ui/core/Box';
// import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
// import Typography from '@material-ui/core/Typography';
// import { makeStyles } from '@material-ui/core/styles';
// import Container from '@material-ui/core/Container';
// import MaterialLink from '@material-ui/core/Link';
// import AddressInput fro../auth/AddressInputput';
// import MuiPhoneNumber from 'material-ui-phone-number';
// import SignIn from './SignIn';

// function Copyright() {
//   return (
//     <Typography variant="body2" color="textSecondary" align="center">
//       {'Copyright © '}
//       <Router>
//         <MaterialLink color="inherit" href="https://material-ui.com/">
//           King Flyer
//         </MaterialLink>{' '}
//       </Router>
//       {new Date().getFullYear()}
//       {'.'}
//     </Typography>
//   );
// }

// const useStyles = makeStyles((theme) => ({
//   paper: {
//     marginTop: theme.spacing(8),
//     display: 'flex',
//     flexDirection: 'column',
//     alignItems: 'center',
//   },
//   avatar: {
//     margin: theme.spacing(1),
//     backgroundColor: theme.palette.secondary.main,
//   },
//   form: {
//     width: '100%', 
//     marginTop: theme.spacing(3),
//   },
//   submit: {
//     margin: theme.spacing(3, 0, 2),
//   },
// }));

// export default function SignUp() {
//   const classes = useStyles();
//   const [address,setAddress] = useState(0);
//   const [addresses,setAddresses] = useState([]);
//   const [user , setUser] = useState({
//     userName: "",
//     password: "",
//     firstName: "",
//     lastName: "",
//     contact: {
//         address: {
//             type: "",
//             addressLine: "",
//             zipCode: 0,
//             city: "",
//             state: "",
//             country: ""
//         },
//         email: "",
//         mobileNo: ""
//     }
//   })
// /*
//   function validateForm() {
//     return user.userName.length > 0 && user.password.length > 0 && user.firstName.length >0 && user.lastName.length >0 && user.mobile !== 0 && user.email.length >0;
//   }
// */
//   function handleAddAddress(address) {
//       setUser({...user , contact :{ ...user.contact , address: {
//           type: "current",
//           addressLine : address.addressLine1 +' , '+ address.addressLine2,
//           zipCode: address.zip,
//           city: address.city,
//           state: address.region,
//           country: address.country
//       }}})
//       setAddresses([address]);
//   }

//   function handleChangeAddress(addressIndex) {
//       setAddress(addressIndex);
//   }

//   function handleOnChangeMobile(mobile){
//       setUser({...user, contact :{ ...user.contact , mobileNo: mobile}})
//   }

//   function handleSubmit(event){
//     event.preventDefault();
//     console.log(user);
//     Axios.post('http://localhost:8765/user-service/user/registration', user).catch(
//         (error)=>{
//             console.log(error.response.data);
//         }
//     );
//   }

//   return (
    
//     <Container component="main" maxWidth="xs">
//       <CssBaseline />
//       <div className={classes.paper}>
//         <Avatar className={classes.avatar}>
//           <LockOutlinedIcon />
//         </Avatar>
//         <Typography component="h1" variant="h5">
//           Sign up
//         </Typography>
//         <form onSubmit={handleSubmit} className={classes.form} noValidate>
//           <Grid container spacing={2}>
//             <Grid item xs={12}>
//               <TextField
//                 variant="outlined"
//                 required
//                 fullWidth
//                 id="username"
//                 label="Username"
//                 name="username"
//                 onChange={ e => setUser({ ...user, userName: e.target.value})}
//               />
//             </Grid>
//             <Grid item xs={12}>
//               <TextField
//                 variant="outlined"
//                 required
//                 fullWidth
//                 name="password"
//                 label="Password"
//                 type="password"
//                 id="password"
//                 autoComplete="current-password"
//                 onChange={ e => setUser({ ...user, password: e.target.value})}
//               />
//             </Grid>
//             <Grid item xs={12} sm={6}>
//               <TextField
//                 autoComplete="fname"
//                 name="firstName"
//                 variant="outlined"
//                 required
//                 fullWidth
//                 id="firstName"
//                 label="First Name"
//                 autoFocus
//                 onChange={ e => setUser({ ...user, firstName: e.target.value})}
//               />
//             </Grid>
//             <Grid item xs={12} sm={6}>
//               <TextField
//                 variant="outlined"
//                 required
//                 fullWidth
//                 id="lastName"
//                 label="Last Name"
//                 name="lastName"
//                 autoComplete="lname"
//                 onChange={ e => setUser({ ...user, lastName: e.target.value})}
//               />
//             </Grid>
//             <Grid item xs={12}>
//             <AddressInput
//                 onAdd={handleAddAddress}
//                 onChange={handleChangeAddress}
//                 value={address}
//                 allAddresses={addresses}
//             />
//             </Grid>
//             <Grid item xs={12}>
//               <TextField
//                 variant="outlined"
//                 required
//                 fullWidth
//                 id="email"
//                 label="Email Address"
//                 name="email"
//                 autoComplete="email"
//                 onChange={ e => setUser({ ...user , contact : {...user.contact, email : e.target.value } })}
//               />
//             </Grid>
//             <Grid item xs={12}>
//                 <MuiPhoneNumber defaultCountry={'in'} onChange={handleOnChangeMobile}/>
//             </Grid>
//           </Grid>
//           <Button
//             type="submit"
//             fullWidth
//             variant="contained"
//             color="primary"
//             className={classes.submit}
//             /*disabled={!validateForm()}*/
//           >
//             Sign Up
//           </Button>
//           <Grid container justify="flex-end">
//             <Grid item>
//               <Link to="/signin">
//                 Already have an account? Sign in
//               </Link>
//             </Grid>
//           </Grid>
//         </form>
//       </div>
//       <Box mt={5}>
//         <Copyright />
//       </Box>
//     </Container>
//   );
// }