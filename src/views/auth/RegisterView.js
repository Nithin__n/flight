import React,{useState} from 'react'
import Axios from 'axios';
import Alert from '@material-ui/lab/Alert';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import MaterialLink from '@material-ui/core/Link';
import AddressInput from './AddressInput';
import MuiPhoneNumber from 'material-ui-phone-number';
import { Link , useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { Formik } from 'formik';
import Page from 'src/components/Page';
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', 
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  }
}));

// function Copyright() {
//   return (
//     <Typography variant="body2" color="textSecondary" align="center">
//       {'Copyright © '}
//       <Router>
//         <MaterialLink color="inherit" href="https://material-ui.com/">
//           King Flyer
//         </MaterialLink>{' '}
//       </Router>
//       {new Date().getFullYear()}
//       {'.'}
//     </Typography>
//   );
// }

const RegisterView = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const [address,setAddress] = useState(0);
  const [addresses,setAddresses] = useState([]);
  const [status, setStatus] = useState(true);
  const [user , setUser] = useState({
    userName: "",
    password: "",
    firstName: "",
    lastName: "",
    contact: {
        address: {
            type: "",
            addressLine: "",
            zipCode: 0,
            city: "",
            state: "",
            country: ""
        },
        email: "",
        mobileNo: ""
    }
  })

  function setIncorrect(){
      setStatus(false);
  }

  function setCorrect(){
      setStatus(true);
  }


  function validateEmail(email){
    if (typeof email !== "undefined") {
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    
      if (pattern.test(email)) {
        return true;
      }
  }
  return false;

}


  function validateForm() {
    return user.userName.length > 0 && user.password.length > 0 && user.firstName.length >0 && user.lastName.length >0 && user.contact.mobileNo.length==15 && validateEmail(user.contact.email) && validateAddress() ;
  }

  function validateAddress(){ 
    return user.contact.address.type.length>0 && user.contact.address.addressLine.length>0 && user.contact.address.zipCode!=0 && user.contact.address.city.length>0 && user.contact.address.state.length>0 && user.contact.address.country.length>0;
  }

  function handleAddAddress(address) {
      setUser({...user , contact :{ ...user.contact , address: {
          type: "current",
          addressLine : address.addressLine1 +' , '+ address.addressLine2,
          zipCode: address.zip,
          city: address.city,
          state: address.region,
          country: address.country
      }}})
      setAddresses([address]);
  }

  function handleChangeAddress(addressIndex) {
      setAddress(addressIndex);
  }

  function handleOnChangeMobile(mobile){
      setUser({...user, contact :{ ...user.contact , mobileNo: mobile}})
  }

  function handleSubmit(event){
    event.preventDefault();
    console.log(user);
    Axios.post('http://localhost:8765/user-service/user/registration', user)
    .then(()=>{
      navigate('/login', { replace: true });
    })
    .catch(
        (error)=>{
            console.log(error.response.data);
            setIncorrect();
        }
    );
  }
  return (
    <Page
      className={classes.root}
      title="Register"
    >
      <Box
        display="flex"
        flexDirection="column"
        height="100%"
        justifyContent="center"
      >
    
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form onSubmit={handleSubmit} className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="username"
                label="Username"
                name="username"
                onChange={ e => {setUser({ ...user, userName: e.target.value}); setCorrect()}}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={ e => setUser({ ...user, password: e.target.value})}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="First Name"
                autoFocus
                onChange={ e => setUser({ ...user, firstName: e.target.value})}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Last Name"
                name="lastName"
                autoComplete="lname"
                onChange={ e => setUser({ ...user, lastName: e.target.value})}
              />
            </Grid>
            <Grid item xs={12}>
            <AddressInput
                onAdd={handleAddAddress}
                onChange={handleChangeAddress}
                value={address}
                allAddresses={addresses}
            />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                onChange={ e => setUser({ ...user , contact : {...user.contact, email : e.target.value } })}
              />
            </Grid>
            <Grid item xs={12}>
                <MuiPhoneNumber defaultCountry={'in'} onChange={handleOnChangeMobile}/>
            </Grid>
          </Grid>
          { (!status) && <Alert severity="error">Username already exists. Please try another username</Alert>}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={!validateForm()}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link to="/login">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        {/* <Copyright /> */}
      </Box>
    </Container>
      </Box>
    </Page>
  );
};

export default RegisterView;



// import React,{useState} from 'react'
// import Axios from 'axios';
// import Avatar from '@material-ui/core/Avatar';
// import Button from '@material-ui/core/Button';
// import CssBaseline from '@material-ui/core/CssBaseline';
// import TextField from '@material-ui/core/TextField';
// import Grid from '@material-ui/core/Grid';
// import Box from '@material-ui/core/Box';
// import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
// import Typography from '@material-ui/core/Typography';
// import { makeStyles } from '@material-ui/core/styles';
// import Container from '@material-ui/core/Container';
// import MaterialLink from '@material-ui/core/Link';
// import AddressInput from './AddressInput';
// import MuiPhoneNumber from 'material-ui-phone-number';
// import { Link , useNavigate } from 'react-router-dom';
// import * as Yup from 'yup';
// import { Formik } from 'formik';
// import Page from 'src/components/Page';
// const useStyles = makeStyles((theme) => ({
//   root: {
//     backgroundColor: theme.palette.background.dark,
//     height: '100%',
//     paddingBottom: theme.spacing(3),
//     paddingTop: theme.spacing(3)
//   },
//   paper: {
//     marginTop: theme.spacing(8),
//     display: 'flex',
//     flexDirection: 'column',
//     alignItems: 'center',
//   },
//   avatar: {
//     margin: theme.spacing(1),
//     backgroundColor: theme.palette.secondary.main,
//   },
//   form: {
//     width: '100%', 
//     marginTop: theme.spacing(3),
//   },
//   submit: {
//     margin: theme.spacing(3, 0, 2),
//   }
// }));

// // function Copyright() {
// //   return (
// //     <Typography variant="body2" color="textSecondary" align="center">
// //       {'Copyright © '}
// //       <Router>
// //         <MaterialLink color="inherit" href="https://material-ui.com/">
// //           King Flyer
// //         </MaterialLink>{' '}
// //       </Router>
// //       {new Date().getFullYear()}
// //       {'.'}
// //     </Typography>
// //   );
// // }

// const RegisterView = () => {
//   const classes = useStyles();
//   const navigate = useNavigate();
//   const [address,setAddress] = useState(0);
//   const [addresses,setAddresses] = useState([]);
//   const [user , setUser] = useState({
//     userName: "",
//     password: "",
//     firstName: "",
//     lastName: "",
//     contact: {
//         address: {
//             type: "",
//             addressLine: "",
//             zipCode: 0,
//             city: "",
//             state: "",
//             country: ""
//         },
//         email: "",
//         mobileNo: ""
//     }
//   })
// /*
//   function validateForm() {
//     return user.userName.length > 0 && user.password.length > 0 && user.firstName.length >0 && user.lastName.length >0 && user.mobile !== 0 && user.email.length >0;
//   }
// */
//   function handleAddAddress(address) {
//       setUser({...user , contact :{ ...user.contact , address: {
//           type: "current",
//           addressLine : address.addressLine1 +' , '+ address.addressLine2,
//           zipCode: address.zip,
//           city: address.city,
//           state: address.region,
//           country: address.country
//       }}})
//       setAddresses([address]);
//   }

//   function handleChangeAddress(addressIndex) {
//       setAddress(addressIndex);
//   }

//   function handleOnChangeMobile(mobile){
//       setUser({...user, contact :{ ...user.contact , mobileNo: mobile}})
//   }

//   function handleSubmit(event){
//     event.preventDefault();
//     console.log(user);
//     Axios.post('http://localhost:8765/user-service/user/registration', user)
//     .then((response)=>{
//       navigate('/login', { replace: true });
//     })
//     .catch(
//         (error)=>{
//             console.log(error.response);
//         }
//     );
//   }
//   return (
//     <Page
//       className={classes.root}
//       title="Register"
//     >
//       <Box
//         display="flex"
//         flexDirection="column"
//         height="100%"
//         justifyContent="center"
//       >
    
//     <Container component="main" maxWidth="xs">
//       <CssBaseline />
//       <div className={classes.paper}>
//         <Avatar className={classes.avatar}>
//           <LockOutlinedIcon />
//         </Avatar>
//         <Typography component="h1" variant="h5">
//           Sign up
//         </Typography>
//         <form onSubmit={handleSubmit} className={classes.form} noValidate>
//           <Grid container spacing={2}>
//             <Grid item xs={12}>
//               <TextField
//                 variant="outlined"
//                 required
//                 fullWidth
//                 id="username"
//                 label="Username"
//                 name="username"
//                 onChange={ e => setUser({ ...user, userName: e.target.value})}
//               />
//             </Grid>
//             <Grid item xs={12}>
//               <TextField
//                 variant="outlined"
//                 required
//                 fullWidth
//                 name="password"
//                 label="Password"
//                 type="password"
//                 id="password"
//                 autoComplete="current-password"
//                 onChange={ e => setUser({ ...user, password: e.target.value})}
//               />
//             </Grid>
//             <Grid item xs={12} sm={6}>
//               <TextField
//                 autoComplete="fname"
//                 name="firstName"
//                 variant="outlined"
//                 required
//                 fullWidth
//                 id="firstName"
//                 label="First Name"
//                 autoFocus
//                 onChange={ e => setUser({ ...user, firstName: e.target.value})}
//               />
//             </Grid>
//             <Grid item xs={12} sm={6}>
//               <TextField
//                 variant="outlined"
//                 required
//                 fullWidth
//                 id="lastName"
//                 label="Last Name"
//                 name="lastName"
//                 autoComplete="lname"
//                 onChange={ e => setUser({ ...user, lastName: e.target.value})}
//               />
//             </Grid>
//             <Grid item xs={12}>
//             <AddressInput
//                 onAdd={handleAddAddress}
//                 onChange={handleChangeAddress}
//                 value={address}
//                 allAddresses={addresses}
//             />
//             </Grid>
//             <Grid item xs={12}>
//               <TextField
//                 variant="outlined"
//                 required
//                 fullWidth
//                 id="email"
//                 label="Email Address"
//                 name="email"
//                 autoComplete="email"
//                 onChange={ e => setUser({ ...user , contact : {...user.contact, email : e.target.value } })}
//               />
//             </Grid>
//             <Grid item xs={12}>
//                 <MuiPhoneNumber defaultCountry={'in'} onChange={handleOnChangeMobile}/>
//             </Grid>
//           </Grid>
//           <Button
//             type="submit"
//             fullWidth
//             variant="contained"
//             color="primary"
//             className={classes.submit}
//             /*disabled={!validateForm()}*/
//           >
//             Sign Up
//           </Button>
//           <Grid container justify="flex-end">
//             <Grid item>
//               <Link to="/login">
//                 Already have an account? Sign in
//               </Link>
//             </Grid>
//           </Grid>
//         </form>
//       </div>
//       <Box mt={5}>
//         {/* <Copyright /> */}
//       </Box>
//     </Container>
//       </Box>
//     </Page>
//   );
// };

// export default RegisterView;



// // import React from 'react';
// // import { Link as RouterLink, useNavigate } from 'react-router-dom';
// // import * as Yup from 'yup';
// // import { Formik } from 'formik';
// // import {
// //   Box,
// //   Button,
// //   Checkbox,
// //   Container,
// //   FormHelperText,
// //   Link,
// //   TextField,
// //   Typography,
// //   makeStyles
// // } from '@material-ui/core';
// // import Page from 'src/components/Page';

// // const useStyles = makeStyles((theme) => ({
// //   root: {
// //     backgroundColor: theme.palette.background.dark,
// //     height: '100%',
// //     paddingBottom: theme.spacing(3),
// //     paddingTop: theme.spacing(3)
// //   }
// // }));

// // const RegisterView = () => {
// //   const classes = useStyles();
// //   const navigate = useNavigate();

// //   return (
// //     <Page
// //       className={classes.root}
// //       title="Register"
// //     >
// //       <Box
// //         display="flex"
// //         flexDirection="column"
// //         height="100%"
// //         justifyContent="center"
// //       >
// //         <Container maxWidth="sm">
// //           <Formik
// //             initialValues={{
// //               email: '',
// //               firstName: '',
// //               lastName: '',
// //               password: '',
// //               policy: false
// //             }}
// //             validationSchema={
// //               Yup.object().shape({
// //                 email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
// //                 firstName: Yup.string().max(255).required('First name is required'),
// //                 lastName: Yup.string().max(255).required('Last name is required'),
// //                 password: Yup.string().max(255).required('password is required'),
// //                 policy: Yup.boolean().oneOf([true], 'This field must be checked')
// //               })
// //             }
// //             onSubmit={() => {
// //               navigate('/app/dashboard', { replace: true });
// //             }}
// //           >
// //             {({
// //               errors,
// //               handleBlur,
// //               handleChange,
// //               handleSubmit,
// //               isSubmitting,
// //               touched,
// //               values
// //             }) => (
// //               <form onSubmit={handleSubmit}>
// //                 <Box mb={3}>
// //                   <Typography
// //                     color="textPrimary"
// //                     variant="h2"
// //                   >
// //                     Create new account
// //                   </Typography>
// //                   <Typography
// //                     color="textSecondary"
// //                     gutterBottom
// //                     variant="body2"
// //                   >
// //                     Use your email to create new account
// //                   </Typography>
// //                 </Box>
// //                 <TextField
// //                   error={Boolean(touched.firstName && errors.firstName)}
// //                   fullWidth
// //                   helperText={touched.firstName && errors.firstName}
// //                   label="First name"
// //                   margin="normal"
// //                   name="firstName"
// //                   onBlur={handleBlur}
// //                   onChange={handleChange}
// //                   value={values.firstName}
// //                   variant="outlined"
// //                 />
// //                 <TextField
// //                   error={Boolean(touched.lastName && errors.lastName)}
// //                   fullWidth
// //                   helperText={touched.lastName && errors.lastName}
// //                   label="Last name"
// //                   margin="normal"
// //                   name="lastName"
// //                   onBlur={handleBlur}
// //                   onChange={handleChange}
// //                   value={values.lastName}
// //                   variant="outlined"
// //                 />
// //                 <TextField
// //                   error={Boolean(touched.email && errors.email)}
// //                   fullWidth
// //                   helperText={touched.email && errors.email}
// //                   label="Email Address"
// //                   margin="normal"
// //                   name="email"
// //                   onBlur={handleBlur}
// //                   onChange={handleChange}
// //                   type="email"
// //                   value={values.email}
// //                   variant="outlined"
// //                 />
// //                 <TextField
// //                   error={Boolean(touched.password && errors.password)}
// //                   fullWidth
// //                   helperText={touched.password && errors.password}
// //                   label="Password"
// //                   margin="normal"
// //                   name="password"
// //                   onBlur={handleBlur}
// //                   onChange={handleChange}
// //                   type="password"
// //                   value={values.password}
// //                   variant="outlined"
// //                 />
// //                 <Box
// //                   alignItems="center"
// //                   display="flex"
// //                   ml={-1}
// //                 >
// //                   <Checkbox
// //                     checked={values.policy}
// //                     name="policy"
// //                     onChange={handleChange}
// //                   />
// //                   <Typography
// //                     color="textSecondary"
// //                     variant="body1"
// //                   >
// //                     I have read the
// //                     {' '}
// //                     <Link
// //                       color="primary"
// //                       component={RouterLink}
// //                       to="#"
// //                       underline="always"
// //                       variant="h6"
// //                     >
// //                       Terms and Conditions
// //                     </Link>
// //                   </Typography>
// //                 </Box>
// //                 {Boolean(touched.policy && errors.policy) && (
// //                   <FormHelperText error>
// //                     {errors.policy}
// //                   </FormHelperText>
// //                 )}
// //                 <Box my={2}>
// //                   <Button
// //                     color="primary"
// //                     disabled={isSubmitting}
// //                     fullWidth
// //                     size="large"
// //                     type="submit"
// //                     variant="contained"
// //                   >
// //                     Sign up now
// //                   </Button>
// //                 </Box>
// //                 <Typography
// //                   color="textSecondary"
// //                   variant="body1"
// //                 >
// //                   Have an account?
// //                   {' '}
// //                   <Link
// //                     component={RouterLink}
// //                     to="/login"
// //                     variant="h6"
// //                   >
// //                     Sign in
// //                   </Link>
// //                 </Typography>
// //               </form>
// //             )}
// //           </Formik>
// //         </Container>
// //       </Box>
// //     </Page>
// //   );
// // };

// // export default RegisterView;
