import React,{useState} from 'react';
import {  Link} from 'react-router-dom'
import {  useNavigate } from 'react-router-dom';
import Axios from 'axios';
import Alert from '@material-ui/lab/Alert';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import MaterialLink from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Dashboard } from '@material-ui/icons';
import Page from 'src/components/Page';
import {connect} from 'react-redux'
import {addUser } from '../../js/Action/index'
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <MaterialLink color="inherit" href="#">
        King Flyer
      </MaterialLink>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

function mapDispatchToProps(dispatch) {
  return {
    addUser: (type, user) => dispatch(addUser(type, user))
  };
}
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', 
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  }
}));
let userData= {};

const setData=(data)=>{
userData={...data, ...data.contact, ...data.contact.address}
return userData;
}
const LoginForm = (props) => {
  const classes = useStyles();
  const navigate = useNavigate();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [status, setStatus] = useState(true);
  // const [add, setAdd]=useState({})
  // const [data , setData] = useState({
  //     userName: "",
  //     password: "",
  //     firstName: "",
  //     lastName: "",
  //     contact: {
  //         address: {
  //             type: "",
  //             addressLine: "",
  //             zipCode: 0,
  //             city: "",
  //             state: "",
  //             country: ""
  //         },
  //         email: "",
  //         mobileNo: ""
  //   }
  // });

  function validateForm() {
    return username.length > 0 && password.length > 0;
  }

  function setIncorrect(){
      setStatus(false);
  }

  function setCorrect(){
      setStatus(true);
  }
  let address = {}
  async  function handleSubmit(event) {
    event.preventDefault();
   await  Axios.get('http://localhost:8765/user-service/user/login/'+username+'/'+password).then(
        (response)=>{
            setData(response.data)
            console.log(userData)
    //         const serializedState = JSON.stringify({ });
    // localStorage.setItem('state', serializedState);
    window.localStorage.clear();
    props.addUser('LOGOUT', { })
    console.log('removed')
            props.addUser('ADD_USER', userData)
            console.log('aadedd')
            navigate('/app/dashboard', { replace: true });
        }).catch(
            (error)=>{
                console.log(error.response);
                setIncorrect();
            }
        )
  }

  return (
    <Page
      className={classes.root}
      title="Login"
    >
      <Box
        display="flex"
        flexDirection="column"
        height="100%"
        justifyContent="center"
      >
      
      <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form onSubmit={handleSubmit} className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoFocus
            onChange={e => {setUsername(e.target.value); setCorrect()}}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={e => {setPassword(e.target.value) ; setCorrect()}}
          />
          { (!status) && <Alert severity="error">Incorrect credentials. Please try again</Alert>}
          <Button 
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={!validateForm()}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link to="/reset" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link to='/register'>
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
      
      </Box>
    </Page>
  );
};

const LoginView= connect(null, mapDispatchToProps)(LoginForm)
export default LoginView




// import React from 'react';
// import { Link as RouterLink, useNavigate } from 'react-router-dom';
// import * as Yup from 'yup';
// import { Formik } from 'formik';
// import {
//   Box,
//   Button,
//   Container,
//   Grid,
//   Link,
//   TextField,
//   Typography,
//   makeStyles
// } from '@material-ui/core';
// import FacebookIcon from 'src/icons/Facebook';
// import GoogleIcon from 'src/icons/Google';
// import Page from 'src/components/Page';

// const useStyles = makeStyles((theme) => ({
//   root: {
//     backgroundColor: theme.palette.background.dark,
//     height: '100%',
//     paddingBottom: theme.spacing(3),
//     paddingTop: theme.spacing(3)
//   }
// }));

// const LoginView = () => {
//   const classes = useStyles();
//   const navigate = useNavigate();

//   return (
//     <Page
//       className={classes.root}
//       title="Login"
//     >
//       <Box
//         display="flex"
//         flexDirection="column"
//         height="100%"
//         justifyContent="center"
//       >
//         <Container maxWidth="sm">
//           <Formik
//             initialValues={{
//               email: 'demo@devias.io',
//               password: 'Password123'
//             }}
//             validationSchema={Yup.object().shape({
//               email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
//               password: Yup.string().max(255).required('Password is required')
//             })}
//             onSubmit={() => {
//               navigate('/app/dashboard', { replace: true });
//             }}
//           >
//             {({
//               errors,
//               handleBlur,
//               handleChange,
//               handleSubmit,
//               isSubmitting,
//               touched,
//               values
//             }) => (
//               <form onSubmit={handleSubmit}>
//                 <Box mb={3}>
//                   <Typography
//                     color="textPrimary"
//                     variant="h2"
//                   >
//                     Sign in
//                   </Typography>
//                   <Typography
//                     color="textSecondary"
//                     gutterBottom
//                     variant="body2"
//                   >
//                     Sign in on the internal platform
//                   </Typography>
//                 </Box>
//                 <Grid
//                   container
//                   spacing={3}
//                 >
//                   <Grid
//                     item
//                     xs={12}
//                     md={6}
//                   >
//                     <Button
//                       color="primary"
//                       fullWidth
//                       startIcon={<FacebookIcon />}
//                       onClick={handleSubmit}
//                       size="large"
//                       variant="contained"
//                     >
//                       Login with Facebook
//                     </Button>
//                   </Grid>
//                   <Grid
//                     item
//                     xs={12}
//                     md={6}
//                   >
//                     <Button
//                       fullWidth
//                       startIcon={<GoogleIcon />}
//                       onClick={handleSubmit}
//                       size="large"
//                       variant="contained"
//                     >
//                       Login with Google
//                     </Button>
//                   </Grid>
//                 </Grid>
//                 <Box
//                   mt={3}
//                   mb={1}
//                 >
//                   <Typography
//                     align="center"
//                     color="textSecondary"
//                     variant="body1"
//                   >
//                     or login with email address
//                   </Typography>
//                 </Box>
//                 <TextField
//                   error={Boolean(touched.email && errors.email)}
//                   fullWidth
//                   helperText={touched.email && errors.email}
//                   label="Email Address"
//                   margin="normal"
//                   name="email"
//                   onBlur={handleBlur}
//                   onChange={handleChange}
//                   type="email"
//                   value={values.email}
//                   variant="outlined"
//                 />
//                 <TextField
//                   error={Boolean(touched.password && errors.password)}
//                   fullWidth
//                   helperText={touched.password && errors.password}
//                   label="Password"
//                   margin="normal"
//                   name="password"
//                   onBlur={handleBlur}
//                   onChange={handleChange}
//                   type="password"
//                   value={values.password}
//                   variant="outlined"
//                 />
//                 <Box my={2}>
//                   <Button
//                     color="primary"
//                     disabled={isSubmitting}
//                     fullWidth
//                     size="large"
//                     type="submit"
//                     variant="contained"
//                   >
//                     Sign in now
//                   </Button>
//                 </Box>
//                 <Typography
//                   color="textSecondary"
//                   variant="body1"
//                 >
//                   Don&apos;t have an account?
//                   {' '}
//                   <Link
//                     component={RouterLink}
//                     to="/register"
//                     variant="h6"
//                   >
//                     Sign up
//                   </Link>
//                 </Typography>
//               </form>
//             )}
//           </Formik>
//         </Container>
//       </Box>
//     </Page>
//   );
// };

// export default LoginView;
